from ctypes import util
import http_utils as utils
import random
import traceback
import jwt


from engineio.payload import Payload
from flask_cors import CORS
from flask.app import Flask
from flask.globals import request
from flask_socketio import SocketIO, emit
from graph_db import Database
from data_structure import *
from user import User
from flask_socketio import close_room, emit, join_room, leave_room


Payload.max_decode_packets = 1000

app = Flask(__name__)
import http_service # do not move up with other import
CORS(app, support_credentials=True)
app.config['SECRET_KEY'] = 12345 # random bytes not working
#socketio = SocketIO(app, cors_allowed_origins=['http://localhost:4200'], ping_timeout=12000)
socketio = SocketIO(app, cors_allowed_origins="*", ping_timeout=12000)




##################
# EVENT HANDLERS #
##################

@socketio.on('joinMatrixRoom')
def on_join_matrix(data: dict, **kwargs):
    print("Joined", data["roomName"])

    resp = utils.get_resp()
    

    try:
        encoded_jwt = data["jwt"]
        email = jwt.decode(encoded_jwt, "secret", algorithms=["HS256"]).get('email')
        participant = utils.is_participant_connected(http_service.connected_participants, email)
        if utils.is_participant_connected(http_service.connected_participants, email) != None:
            join_room(data["roomName"], request.sid)
            pass
        
        else:
            resp['code'] = 2
            resp['message'] = "User is not in connected list"

    
    except Exception as e:
        resp['code'] = 3
        resp['message'] = f'An error occured:\n {e}.'
        traceback.print_exc()
        
    
    emit('joinMatrixRoom', resp)
   

@socketio.on('leaveMatrixRoom')
def on_leave_matrix(data: dict, **kwargs):
    print("Left", data["roomName"])
    resp = utils.get_resp()
    

    try:
        encoded_jwt = data["jwt"]
        email = jwt.decode(encoded_jwt, "secret", algorithms=["HS256"]).get('email')
        if utils.is_participant_connected(http_service.connected_participants, email) != None:
            leave_room(data['roomName'], request.sid)

        
        else:
            resp['code'] = 2
            resp['message'] = "User is not in connected list"

    
    except Exception as e:
        resp['code'] = 3
        resp['message'] = f'An error occured:\n {e}.'
        traceback.print_exc()
        
    

    emit('leaveMatrixRoom', resp)
    
   
@socketio.on('newPostItCreated')
def on_new_postit_created(data: dict, **kwargs):
    obj = Object(data['data']['text'], data['data']['zone'], data['data']['x'], data['data']['y'], data['data']['cssClass'], False)
    db.create_node(obj)
    data["data"] = obj.get_dictionary_property()
    link = ObjectCartographyRelation()
    db.create_relation(data['roomName'], obj.id, link)
    emit('newPostItCreated', {
             'code': 1, 'data': data["data"]}, room=data["roomName"])
    
    
@socketio.on('dragStartPostIt')   
def on_drag_start_post_it(data: dict, **kwargs):
    emit('dragStartPostIt', {
             'code': 1, 'data': data["data"]})

@socketio.on('draggingPostIt')
def on_drag_post_it(data: dict, **kwargs):

    emit('draggingPostIt', {
             'code': 1, 'data': data["data"]}, room=data["roomName"])
    
@socketio.on('dragEndPostIt')   
def on_drag__end_post_it(data: dict, **kwargs):
    obj = db.get_node(data['data']['id'])
    obj['zone'] = data['data']['zone']
    obj['x'] = data['data']['x']
    obj['y'] = data['data']['y']
    db.update_node_property(obj)
    emit('dragEndPostIt', {
             'code': 1, 'data': data["data"]}, room=data["roomName"])
    
@socketio.on('startWeightingState')
def on_start_weighting_state(data: dict, **kwargs):
    db.set_state_status(data['data']['carto_id'], State.weighting.value)
    emit('startWeightingState')
    
@socketio.on('endWeightingState')
def on_end_weighting_state(data: dict, **kwargs):
    db.set_state_status(data['data']['carto_id'], State.view.value)
    emit('endWeightingState')

@socketio.on('createWeighting')
def on_create_weighting(data: dict, **kwargs):
    list_of_object_id = db.get_object_in_zone(data['data']['carto_id'], data['data']['zone'])
    # get all object id from one zone
    weighting = Weighting(data['data']['question'], data['data']['rules'], data['data']['filters'], 
                          data['data']['zone'], data['data']['nb_seconds'], data['data']['list_label_participant'], data['data']['type'])
    db.create_node(weighting)
    link = WeightingObjectRelation()
    link_carto = WeightingCartographyRelation()
    db.create_relation(data['data']['carto_id'], weighting.id, link_carto)
    for id in list_of_object_id:
        db.create_relation(weighting.id, id, link)
        


@socketio.on('vote')
def on_vote(data: dict, **kwargs):
    participant_id = data['data']['participant_id']
    weighting_id = data['data']['weighting_id']
    link = WeightingParticipantRelation()
    link.info['information'] = data['data']['information'].copy()
    link.info['vote'] = data['data']['vote'].copy()
    db.create_relation(participant_id, weighting_id, link)

   
@socketio.on('test')
def on_create_user(data: dict, **kwargs):
    print(http_service.connected_participants[0].email)
    print("The Socket works!")


@socketio.on('authenticate_user')
def on_authenticate_user(data: dict, **kwargs):
    """ Authenticate a user """
    pass




if __name__ == '__main__':
    print('started')
    
    db = Database("neo4j+s://81e26a1d.databases.neo4j.io", "neo4j", "WLlYKpL-SWYYx-YRpMNjDtIHLIDo3KJE43ETASqTOa4")
  
    p1 = Participant('Keshav', 'keshkesh', 'keshav@keshav.com')
    p2 = Participant('Reetesh', 'keshkesh', 'reetesh@reetesh.com')
    p3 = Participant('Admin', 'keshkesh', 'admin@admin.com')
    db.create_node(p3)
    db.create_node(p1)
    db.create_node(p2)
    
    project = Object('Paix',2, 0,0,'redish-postit', True )
    project2 = Object('P',2, 0,0,'redish-postit', False )
    db.create_node(project)
    db.create_node(project2)
    carto = Cartography('Comment obtenir la paix?', project.id )
    db.create_node(carto)
    rel = ObjectCartographyRelation()
    db.create_relation(project.id, carto.id, rel)
    db.create_relation(carto.id, project2.id, rel)
    link = ParticipantAsAccessProject()
    db.create_relation(p1.id, project.id, link)
    db.create_relation(p2.id, project.id, link)
    weigh = Weighting('Comment obtenir la paix',None,None,12,15,None,WeightingType.urgence)
    db.create_node(weigh)
    weilink = WeightingObjectRelation()
    db.create_relation(project.id, weigh.id, weilink)
    
    list_object = db.get_object_in_zone(carto.id, 2)
    print(list_object)

    
    
    
    socketio.run(app, host="0.0.0.0")

    