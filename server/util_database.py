from enum import Enum
     
class NodeType(Enum):
    Error = 'Error'
    Object = 'Object'
    Participant = 'Participant'
    Animator = 'Animator'
    Admin = 'Admin'
    Cartography = 'Cartography'
    Weighting = 'Weighting'
    Organization = 'Organization'
    
class EdgeType(Enum):
    Error = 'Error'
    ObjectCartographyRelation = 'ObjectCartographyRelation'
    ParticipantCartographyRelation = 'ParticipantCartographyRelation'
    ObjectObjectRelation = 'ObjectObjectRelation'
    WeightingObjectRelation = 'WeightingObjectRelation'
    WeightingParticipantRelation = 'WeightingParticipantRelation'
    WeightingWeightingRelation = 'WeightingWeightingRelation'
    CartographyCartographyRelation = 'CartographyCartographyRelation'
    ParticipantAsAccessProject = 'ParticipantAsAccessProject'



node_type_from_id = {
    'O': NodeType.Object, 
    'P': NodeType.Participant,
    'C': NodeType.Cartography, 
    'D': NodeType.Admin, 
    'M': NodeType.Animator, 
    'W': NodeType.Weighting, 
    'G': NodeType.Organization,
    'E': NodeType.Error 
    }

edge_type = {
    'A': EdgeType.ObjectCartographyRelation,
    'B': EdgeType.ParticipantCartographyRelation,
    'C': EdgeType.ObjectObjectRelation,
    'D': EdgeType.WeightingObjectRelation,
    'E': EdgeType.WeightingParticipantRelation,
    'F': EdgeType.WeightingWeightingRelation,
    'G': EdgeType.CartographyCartographyRelation,
    'H': EdgeType.ParticipantAsAccessProject,
    'I': EdgeType.Error 
}
