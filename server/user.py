
from __future__ import annotations

from datetime import date, datetime, time, timedelta

from flask_socketio import leave_room, rooms
from pymongo import mongo_client

from instance import Instance, InstanceList
import pickle


class User(Instance):
    def __init__(self, name, sid) -> None:
        super().__init__(name)
        self.sid = sid
        self.connexion_time = datetime.now()
        self.connexion_time_stamp = self.connexion_time.strftime('%B %d, %Y %H:%M:%S')

    def __del__(self) -> None:
        session_length = self.calculate_session_length()
        pass
    
    def get_avatar(self):
        pass

    def get_first_name(self):
        pass

    def get_last_name(self):
        pass









