
from enum import Enum
import json
from graph_db import Database
import copy
import random as r

class IDGenerator:
    def __init__(self, next_node_id:dict):
        # Id are 2 letters and 6 numbers
        # first letter is N for node ans E for edge
        # second is the type of the node O:object, P:participant, C:cartography, W:weighting
        # ex: 'NO000000' for a N of type object
        print(next_node_id)
        self.__next_object_id = next_node_id['next_object_id']
        self.__next_participant_id = next_node_id['next_participant_id']
        self.__next_cartography_id = next_node_id['next_cartography_id']
        self.__next_weighting_id = next_node_id['next_weighting_id']
        self.__next_organization_id = next_node_id['next_organization_id']
    
    def get_object_id(self)->str:
        return self.__next_object_id
    
    def get_participant_id(self)->str:
        return self.__next_participant_id
    
    def get_cartography_id(self)->str:
        return self.__next_cartography_id
    
    def get_weighting_id(self)->str:
        return self.__next_weighting_id
    
    def get_organization_id(self)->str:
        return self.__next_organization_id
        
    def increment_object_id(self)->str:
        old = self.__next_object_id
        id = self.__next_object_id[2:]
        # convert to integer
        id = int(id)
        # increment
        id += 1
        # convert back to string
        id = old[:2] + str(id)
        # assign to self
        self.__next_object_id = id
        return old
    
    def increment_participant_id(self)->str:
        old = self.__next_participant_id
        id = self.__next_participant_id[2:]
        # convert to integer
        id = int(id)
        # increment
        id += 1
        # convert back to string
        id = old[:2] + str(id)
        # assign to self
        self.__next_participant_id = id
        return old
        
    def increment_cartography_id(self)->str:
        old = self.__next_cartography_id
        id = self.__next_cartography_id[2:]
        # convert to integer
        id = int(id)
        # increment
        id += 1
        # convert back to string
        id = old[:2] + str(id)
        # assign to self
        self.__next_cartography_id = id
        return old
        
    def increment_weighting_id(self)->str:
        old = self.__next_weighting_id
        id = self.__next_weighting_id[2:]
        # convert to integer
        id = int(id)
        # increment
        id += 1
        # convert back to string
        id = old[:2] + str(id)
        # assign to self
        self.__next_weighting_id = id
        return old
        
    def increment_organization_id(self)->str:
        old = self.__next_organization_id
        id = self.__next_organization_id[2:]
        # convert to integer
        id = int(id)
        # increment
        id += 1
        # convert back to string
        id = old[:2] + str(id)
        # assign to self
        self.__next_organization_id = id
        return old
               
        

db = Database("neo4j+s://81e26a1d.databases.neo4j.io", "neo4j", "WLlYKpL-SWYYx-YRpMNjDtIHLIDo3KJE43ETASqTOa4")
db.clear_database()
idGenerator = IDGenerator(db.get_next_id_node())

# can be dictionary or array
def convert_dict_to_string(dictionary: any) -> str:
    return json.dumps(dictionary)

# can be dictionary or array
def convert_string_back_to_dictionary(string_dict: str) -> any:
    return json.loads(string_dict)

def get_class_string(anything: any) -> str:
    class_string = str(anything.__class__)
    class_string = class_string[class_string.find('.')+1 :]
    class_string = class_string[:-2]
    return class_string

class State(Enum):
    weighting = 'weighting',
    divergence = 'divergence',
    view = 'view' # when not in divergence or weighting
    
class WeightingType(Enum):
    urgence = 'urgence',
    importance = 'importance',
    turbulence = 'turbulence'
    
class Edge:
    def __init__(self):
        # key is the feature, value is the value
        self.info = {'type':'I'}
        
        pass

class ObjectCartographyRelation(Edge):
    
    def __init__(self, weight: float=0.0):
        super(ObjectCartographyRelation, self).__init__()
        self.info['type'] = 'A'
        self.info['weight']= weight

class ParticipantCartographyRelation(Edge):
    def __init__(self, info_participant: dict, objects_created: list):
        super(ParticipantCartographyRelation, self).__init__()
        self.info['type'] = 'B'
        self.info['info_participant'] = info_participant # keys are labels and values are the info
        self.info['objects_created'] = objects_created # list containing all id 
        
class ObjectObjectRelation(Edge):
    def __init__(self, weight:float):
        super(ObjectObjectRelation, self).__init__()
        self.info['type'] = 'C'
        self.info['weight'] = weight

class WeightingObjectRelation(Edge):
    def __init__(self, weight: float=0.0):
        super(WeightingObjectRelation, self).__init__()
        self.info['type'] = 'D'
        self.info['weight'] = weight
        
class WeightingParticipantRelation(Edge):
    def __init__(self):
        super(WeightingParticipantRelation, self).__init__()
        self.info['type'] = 'E'
        self.info['votes'] = {} # key is the id of object value is the note
        self.info['information'] = {} # key is the label and value is the info, ex: key: name, value: Anthony
        
class WeightingWeightingRelation(Edge):
    # this is a directed edge meaning that the direction is important
    def __init__(self, previousWeighting: int = None):
        super(WeightingWeightingRelation, self).__init__()
        self.info['type'] = 'F'
        self.info['nextWeighting'] = [] # array of all id of immediate next Weighting
        self.info['previousWeighting'] = previousWeighting # the id of Weighting before, none if it's the first
        
class WeightingCartographyRelation(Edge):
    # this is a directed edge meaning that the direction is important
    def __init__(self):
        super(WeightingWeightingRelation, self).__init__()
        self.info['type'] = 'F'

  
        
class CartographyCartographyRelation(Edge):
    # this is a directed edge meaning that the direction is important
    def __init__(self, previousCartography: str = None):
        super(CartographyCartographyRelation, self).__init__()
        self.info['type'] = 'G'
        self.info['nextCartography'] = [] # array of all id of immediate next Cartography
        self.info['previousCartography'] = previousCartography # the id of Cartography before, none if it's the first

class ParticipantAsAccessProject(Edge):
    def __init__(self):
        super(ParticipantAsAccessProject, self).__init__()
        self.info['type'] = 'H'
class AnimatorOfProject(Edge):
    def __init__(self):
        super(AnimatorOfProject, self).__init__()
class Node:
    def __init__(self, id: str):
        self.id = id
        self.edges = {} # key is the id of neigboor and value is the edge object
        
    def get_dictionary_property(self) -> dict:
        pass
        
    def get_id(self):
        return self.id
    
    # to change Edge for something better, we don't want Edge to be living on it's own
    def add_edge(self, neighbor_id: str, edge: Edge):
        self.edges[neighbor_id] = edge
        
    def __str__(self):
        return self.id + ' adjacent: ' + str([x for x in self.edges.keys()])
    
    def get_connections(self):
        return self.edges.keys()
    
    def get_all_object_cartography_relation(self) -> list:
        list_of_id = []
        for edge_id in self.edges:
            if get_class_string(self.edges[edge_id]) == 'ObjectCartographyRelation':
                list_of_id.append(edge_id)
        return list_of_id
    

class Organization(Node):
    def __init__(self, name: str, info:str="", id:str=None):
        if id == None:
            id = idGenerator.increment_organization_id()
            db.set_next_id_node(idGenerator)
        super(Organization, self).__init__(id)
        self.name = name
        self.security_code = self.create_security_code()
        self.info = info
        # list of all central node of a projects of the organization
        self.projects = []
        idGenerator
    
    # create security code
    def create_security_code(self) -> str:
        security_code = []
        for i in range(1, 9):
            security_code.append(r.randint(0, 9))
        return ''.join(map(str,security_code))

    # projects will be represented in relation
    def get_dictionary_property(self) -> dict:
        return {'id': self.id, 'security_code':self.security_code, 'name': self.name, 'info':self.info}         

class Participant(Node):
    def __init__(self, name: str, password: str, email: str, id:str=None):
        if id == None:
            id = idGenerator.increment_participant_id()
            db.set_next_id_node(idGenerator)
        super(Participant, self).__init__(id)
        self.email = email
        self.name = name
        self.password = password
        # dictionary of all information on participant
        self.__info = {}
    
    def to_json(self):
        participant_dict = copy.deepcopy(self.__dict__)
        participant_dict.pop("password")
        return json.dumps(participant_dict)
        
    def get_dictionary_property(self) -> dict:
        d = {'id': self.id, 'email': self.email, 'name': self.name, 'password': self.password}
        for label in self.__info:
            d[label] = self.__info[label]
        return d
        
    def add_info(self, label: str, info: any):
        self.__info[label] = info
    
    def add_many_info(self, dictionary: dict):
        for key in dictionary.keys():
            self.add_info(key, dictionary[key])
        
    def remove_info(self, label: str):
        if label in self.__info.keys():
            del self.__info[label]
    
    def get_info(self, label: str):
        if label in self.__info.keys():
            return self.__info[label]
        return None
    def get_all_info(self) -> dict:
        return self.__info

    def change_password(self, new_password: str, old_password: str) -> bool:
        if old_password == self.password:
            self.password = new_password
            db.update_node_property(self.get_dictionary_property())
            
            return True
        return False
    
    # return a list of the first node object of every projects
    def get_all_project(self) -> list:
        id_list = []
        for key in self.edges:
            class_string = get_class_string(self.edges[key])
            if class_string == 'ParticipantAsAccessProject':
                id_list.append(key)
        return id_list


# add functionality specific to Admin
class Admin(Participant):
    def __init__(self, name: str, password: str, email: str, id:str=None):
        super(Participant, self).__init__(name, password, email, id)

      # IMPLEMENT THIS FUNCTION IF ATTRIBUTES ARE ADDED
    # def get_dictionary_property(self) -> dict:
    #     d = {'id': self.id, 'email': self.email, 'name': self.name, 'password': self.password}
    #     for label in self.__info:
    #         d[label] = self.__info[label]
    #     return d

        
class Object(Node):
    def __init__(self, text: str, zone: int, x: float, y: float, cssClass: str, isRoot: bool, id:str=None):
        if id == None:
            id = idGenerator.increment_object_id()
            db.set_next_id_node(idGenerator)
        super(Object, self).__init__(id)
        self.zone = zone
        self.text = text
        self.x = x
        self.y = y
        self.cssClass = cssClass
        self.isRoot = isRoot


    def get_dictionary_property(self) -> dict:
        d = {'id': self.id, 'zone': self.zone, 'text': self.text, 'x': self.x, 'y':self.y, 'cssClass': self.cssClass, 'isRoot':self.isRoot}
        return d
        
    
class Cartography(Node):
    
    def __init__(self, question: str, parent_id_object: str, list_label_participant: dict={}, weight: int=0, nb_seconds: int=180, id:str=None):
        if id == None:
            id = idGenerator.increment_cartography_id()
            db.set_next_id_node(idGenerator)
        super(Cartography, self).__init__(id)
        self.cssClass = 'redish-postit'
        self.question = question
        self.nb_seconds = nb_seconds
        self.edges[parent_id_object] = ObjectCartographyRelation(weight)
        self.list_label_participant = list_label_participant # dictionnary of all participant's labels relevante to the question
        self.state = State.view # keep the state of the matrix
    
    # the edges are already represented in the bd
    def get_dictionary_property(self) -> dict:
        d = {'id': self.id, 'question': self.question, 'nb_seconds': self.nb_seconds }
        return d

class Weighting(Node):
    
    def __init__(self, question: str, rules: dict, filters: dict, zone: int, nb_seconds: int, list_label_participant: list, type: WeightingType, id:str=None):
        if id == None:
            id = idGenerator.increment_weighting_id()
            db.set_next_id_node(idGenerator)
        super(Weighting, self).__init__(id)
        self.question = question
        self.rules = rules # the filters to apply
        self.filters = filters # the filters to apply
        self.zone = zone
        self.nb_seconds = nb_seconds # number of seconds to ponderate
        self.list_label_participant = list_label_participant # list of labels
        self.type = type
    
    def get_dictionary_property(self) -> dict:
        d = {'id': self.id, 'question': self.question, 
             'nb_seconds': self.nb_seconds, 'rules': self.rules, 
             'filters': self.filters, 'zone': self.zone, 
             'list_label_participant': self.list_label_participant}
        return d
    
    
class Matrix:
    def __init__(self, root: Object, cartography: Cartography):
        self.root: Object = root
        self.cartography = cartography
        self.postits: Object = []
        

        

        

