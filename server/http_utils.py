import json

#########
# UTILS #
#########

def get_resp(code: int = 1, message: str = "Success", data: dict = None):
    data = data or {}
    resp = {
        'code': code,
        'message': message,
        'data': data
    }
    return resp

def read_json_file():
    with open('mockUsers.json') as json_file:
        return json.load(json_file)

def write_json_file(json_data):
    with open('mockUsers.json', 'w') as json_file:
        json.dump(json_data, json_file)

def is_participant_connected(participant_list, email):
    for participant in participant_list:
        if participant.email == email:
            return participant
    
    return None