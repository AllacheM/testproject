from __main__ import app
from datetime import datetime
from flask.globals import request
from flask_cors import cross_origin
from data_structure import *

import http_utils as utils
import jwt
import traceback

from graph_db import Database


db = Database("neo4j+s://81e26a1d.databases.neo4j.io", "neo4j", "WLlYKpL-SWYYx-YRpMNjDtIHLIDo3KJE43ETASqTOa4")
connected_participants = []
#db.clear_database()

##################
# HTTP REQUESTS #
##################
@app.route('/test')
def success():
    print('welcome')
    return 'welcome'

@app.route('/authenticate_user', methods=['POST'])
@cross_origin(supports_credentials=True)
def authenticate():
    resp = utils.get_resp()
    try:
        # if (request.form["email"] != 'admin' and request.form["password"] != 'admin'):
        #     res = db.get_participant(request.form["email"], request.form["password"])
        # else:
        #     res = Participant('admin', 'admin', 'admin', 'admin@admin.ca')
        
        res = db.get_participant(request.form["email"], request.form["password"])
        
        if res == 1 or res == 2:
            resp['code'] = 2
            resp['message'] = "User authentication failed. Invalid username or password."
            
        else:
            authenticatedParticipant = Participant(res['name'], res['password'], res['email'], res['id'])
            connected_participants.append(authenticatedParticipant)
            encoded_jwt = jwt.encode({"email": request.form["email"]}, "secret", algorithm="HS256")
            resp['data']['user'] = authenticatedParticipant.to_json()
            resp['data']['connexion_time'] = datetime.now().strftime('%B %d, %Y %H:%M:%S')
            resp['data']['jwt'] = encoded_jwt

    
    except Exception as e:
        resp['code'] = 3
        resp['message'] = f'An error occured:\n {e}.'
        traceback.print_exc()
        
    
    return resp

@app.route('/create_user', methods=['POST'])
@cross_origin(supports_credentials=True)
def create_user():
    resp = utils.get_resp()
    
    newParticipant = Participant(request.form["user"], request.form["password"], request.form["email"])
    
    try:
        if db.get_participant(newParticipant.email, newParticipant.password) != 1:
            resp['code'] = 2
            resp['message'] = "This user email already exists"
            
        else:
            db.create_node(newParticipant)
            connected_participants.append(newParticipant)
            encoded_jwt = jwt.encode({"email": request.form["email"]}, "secret", algorithm="HS256")
            resp['data']['user'] = newParticipant.to_json()
            resp['data']['creation_time'] = datetime.now().strftime('%B %d, %Y %H:%M:%S')
            resp['data']['jwt'] = encoded_jwt

    
    except Exception as e:
        resp['code'] = 3
        resp['message'] = f'An error occured:\n {e}.'
        traceback.print_exc()
        
    return resp

@app.route('/get_ponderation/<jwt_token>/<cartography_id>', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_ponderation(jwt_token, cartography_id):
    resp = utils.get_resp()
    
    try:
        email = jwt.decode(jwt_token, "secret", algorithms=["HS256"]).get('email')
        participant =  utils.is_participant_connected(connected_participants, email) 
        if participant != None:
            # get participant access
            ponderations = db.get_ponderations(cartography_id)
            resp['message'] = f"User {email} is in connected list"
            resp['data'] = ponderations

        else:
            resp['code'] = 2
            resp['message'] = "User is not in connected list"

    
    except Exception as e:
        resp['code'] = 3
        resp['message'] = f'An error occured:\n {e}.'
        traceback.print_exc()

    return resp

@app.route('/get_matrix/<jwt_token>/<cartography_id>', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_matrix(jwt_token, cartography_id):
    resp = utils.get_resp()
    
    try:
        email = jwt.decode(jwt_token, "secret", algorithms=["HS256"]).get('email')
        participant =  utils.is_participant_connected(connected_participants, email) 
        if participant != None:
            # get participant access
            matrix = db.get_matrix(cartography_id)
            resp['message'] = f"User {email} is in connected list"
            resp['data'] = matrix

        else:
            resp['code'] = 2
            resp['message'] = "User is not in connected list"

    
    except Exception as e:
        resp['code'] = 3
        resp['message'] = f'An error occured:\n {e}.'
        traceback.print_exc()

    return resp


@app.route('/get_project_trees/<jwt_token>', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_project_trees(jwt_token):
    resp = utils.get_resp()
    
    try:
        email = jwt.decode(jwt_token, "secret", algorithms=["HS256"]).get('email')
        participant =  utils.is_participant_connected(connected_participants, email) 
        if participant != None:
            # get participant access
            id_project_list = db.get_participant_projects(email)
            tree = []
            for id_project in id_project_list:      
                tree.append(db.get_project_tree_by_id(id_project))
            resp['message'] = f"User {email} is in connected list"
            resp['data'] = tree

        else:
            resp['code'] = 2
            resp['message'] = "User is not in connected list"

    
    except Exception as e:
        resp['code'] = 3
        resp['message'] = f'An error occured:\n {e}.'
        traceback.print_exc()

    return resp

@app.route('/get_all_companies', methods=['GET'])
@cross_origin(supports_credentials=True)
def get_all_companies():
    resp = utils.get_resp()
    try:
        all_companies = db.get_all_organizations()
        resp['data']['companies'] = all_companies
    except Exception as e:
        resp['code'] = 3
        resp['message'] = f'An error occured:\n {e}.'
        traceback.print_exc()

    return resp



# projet principal
@app.route('/create_root_project', methods=['POST'])
@cross_origin(supports_credentials=True)
def create_root_project():
    resp = utils.get_resp()

    try:
        encoded_jwt = request.form["jwt"]
        email = jwt.decode(encoded_jwt, "secret", algorithms=["HS256"]).get('email')
        participant =  utils.is_participant_connected(connected_participants, email)
        print(email)
        #participant = db.get_participant_by_email(email)
        print(f'participant: {participant}')
        root = Object(request.form["fonction"], 25, 0,0,'redish-postit', True)
        carto = Cartography(request.form["question"], root.id)
        link = ObjectCartographyRelation(0)
        db.create_node(root)
        db.create_node(carto)
        db.create_relation(root.id, carto.id, link)
        acces = ParticipantAsAccessProject()
        print('acces beeing given')
        print(f'participant: {participant}')
        print(f'root: {root}')
        db.create_relation(participant.id, root.id, acces)
        
        root_tree = {'id': root.id, 'text': root.text, 'cssClass':'redish-postit', 'children': [{'id':carto.id, 'text':carto.question, 'cssClass':'orange-postit', 'children': {}}]}

        resp['data']['tree'] = root_tree

    except Exception as e:
        resp['code'] = 3
        resp['message'] = f'An error occured:\n {e}.'
        traceback.print_exc()
        
    return resp


@app.route('/change_password', methods=['POST'])
@cross_origin(supports_credentials=True)
def change_password():
    resp = utils.get_resp()
    try:
        res = db.get_participant(request.form["email"], request.form["password"])
        if res == 1 or res == 2:
            resp['code'] = 2
            resp['message'] = "Cannot change password. Make sure old password is correct."
            
        else:
            res['password'] = request.form["newpassword"]
            db.update_node_property(res)
    
    except Exception as e:
        resp['code'] = 3
        resp['message'] = f'An error occured:\n {e}.'
        traceback.print_exc()

    return resp


@app.route('/get_user_by_token', methods=['POST'])
@cross_origin(supports_credentials=True)
def get_user_by_token():
    resp = utils.get_resp()
    
    try:
        encoded_jwt = request.form["jwt"]
        email = jwt.decode(encoded_jwt, "secret", algorithms=["HS256"]).get('email')
        participant =  utils.is_participant_connected(connected_participants, email) 
        if utils.is_participant_connected(connected_participants, email) != None:
            resp['message'] = f"User {email} is in connected list"
            resp['data']['connexion_time'] = datetime.now().strftime('%B %d, %Y %H:%M:%S')
            resp['data']['user'] = participant.to_json()

        else:
            resp['code'] = 2
            resp['message'] = "User is not in connected list"

    
    except Exception as e:
        resp['code'] = 3
        resp['message'] = f'An error occured:\n {e}.'
        traceback.print_exc()

    return resp

@app.route('/create_company', methods=['POST'])
@cross_origin(supports_credentials=True)
def create_company():
    resp = utils.get_resp()
    
    try:
        #encoded_jwt = request.form["jwt"]
        #email = jwt.decode(encoded_jwt, "secret", algorithms=["HS256"]).get('email')
        email = request.form['email']
        participant =  utils.is_participant_connected(connected_participants, email)
        if(not db.organization_exists(request.form['name'])):
            organization = Organization(request.form['name'], request.form['info'])
            db.create_node(organization)
            resp['data']['id'] = organization.id
            resp['data']['securityCode'] = organization.security_code
            resp['data']['name'] = organization.name
            resp['data']['info'] = organization.info
            print(resp)
        else:
            resp['code'] = 2
            resp['message'] = f'An organization with this name already exists'

    except Exception as e:
        resp['code'] = 3
        resp['message'] = f'An error occured:\n {e}.'
        traceback.print_exc()
    
    print(resp)
    return resp

@app.route('/disconnect', methods=['POST'])
@cross_origin(supports_credentials=True)
def disconnect():
    resp = utils.get_resp()
    
    try:
        encoded_jwt = request.form["jwt"]
        email = jwt.decode(encoded_jwt, "secret", algorithms=["HS256"]).get('email')
        participant = utils.is_participant_connected(connected_participants, email)
        if participant != None:
            connected_participants.remove(participant)
            resp['code'] = 200
            resp['message'] = f'Participant is disconnected.'
        else:
            raise Exception("Participant was not connected")


    
    except Exception as e:
        resp['code'] = 3
        resp['message'] = f'An error occured:\n {e}.'
        traceback.print_exc()

    return resp
