# PolyExpert Server

## Create Virtual env (Windows) in /server to run server.py:

```
python -m venv vserver
.\vserver\Scripts\activate.bat
pip install -r .\requirements.txt
python3 .\server.py

cd server && .\vserver\Scripts\activate.bat && python3 .\server.py
```
