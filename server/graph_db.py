from neo4j import GraphDatabase
from singleton import Singleton
from util_database import *
import json



    
class Database(metaclass=Singleton):

    def __init__(self, uri: str, user: str, password: str):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))
        self.driver.verify_connectivity()
        print('Database connected!')
            
    # function to close the database when done
    def close(self):
        self.driver.close()
    
    def create_node(self, node):
        with self.driver.session() as session:
            session.write_transaction(self.__create_node, node.get_dictionary_property())
    
    def update_node_property(self, node:dict):
        with self.driver.session() as session:
            session.write_transaction(self.__set_node_properties, node)
            
    def delete_node(self, node_id:str):
        with self.driver.session() as session:
            session.write_transaction(self.__delete_node, node_id)
            
    def create_relation(self, first_node_id:str, second_node_id:str, edge):
        print('relation beeing created')
        with self.driver.session() as session:
            session.write_transaction(self.__create_relation,first_node_id, second_node_id, edge)
            
    def update_relation(self, first_node_id:str, second_node_id:str, edge)->bool:
        with self.driver.session() as session:
            result = session.write_transaction(self.__update_relation,first_node_id, second_node_id, edge)
            return result == None

    
    def delete_relation(self, first_node_id:str, second_node_id:str):
        with self.driver.session() as session:
            session.write_transaction(self.__delete_relation,first_node_id, second_node_id)
    
    def get_participant_by_email(self, email:str)->dict:
        with self.driver.session() as session:
            result = session.read_transaction(self.__get_participant_by_email, email)
            if result == None:
                return 1
            dictionary = {}
            for key in result:
                dictionary[key] = result[key]
            return dictionary
    
        
    def get_node(self, id: str) -> dict:
        with self.driver.session() as session:
            result = session.read_transaction(self.__get_node, id)
            if result == None:
                return 1
            dictionary = {}
            for key in result:
                dictionary[key] = result[key]
            return dictionary
        
    def get_participant(self, email: str, password: str) -> dict:
        with self.driver.session() as session:
            result = session.read_transaction(self.__get_participant, email)
            if result == None:
                return 1
            
            true_password = result['password']
            
            # check password
            if true_password != password:
                return 2
            
            dictionary = {}
            for key in result:
                dictionary[key] = result[key]
            
            return dictionary
        
    def get_participant_projects(self, email:str) -> list:
        with self.driver.session() as session:
            result = session.read_transaction(self.__get_participant_projects,email)
            list_of_project = []
            for id in result:
                list_of_project.append(id['id'])
            return list_of_project
    
    def get_immediate_root(self, node_id: str):
        with self.driver.session() as session:
            result = session.read_transaction(self.__get_immediate_root,node_id)
            dictionary = {}
            for key in result:
                dictionary[key] = result[key]
            return dictionary
    def get_matrix(self, carto_id:str)->dict:
        root = self.get_immediate_root(carto_id)
        objects = self.get_all_cartography_child(carto_id)
        carto = self.get_node(carto_id)
        matrix = {'root': root, 'cartography': carto, 'postits': objects}
        return matrix
        
    def get_all_cartography_child(self, cartography_id: str) -> list:
        with self.driver.session() as session:
            result = session.read_transaction(self.__get_all_cartography_child,cartography_id)
            dictionary = [r.data()['object'] for r in result]
            return dictionary
        
    def get_object_in_zone(self,carto_id: str, zone:int)->list:
        with self.driver.session() as session:
            result = session.read_transaction(self.__get_object_in_zone, carto_id, zone)
            list_of_object = []
            for id in result:
                list_of_object.append(id['object_id'])
            return list_of_object
    
            
    def get_next_id_node(self) -> dict:
        with self.driver.session() as session:
            next_id_dict = session.read_transaction(self.__get_next_id_node)
            dictionary = {}
            for key in next_id_dict:
                dictionary[key] = next_id_dict[key]
            return dictionary

    def set_next_id_node(self, all_id):
        # all_id : a IDGenerator
        with self.driver.session() as session:
            session.write_transaction(self.__set_next_id_node, all_id)
            
    def get_project_tree_by_id(self, project_tree_id: str) -> dict:
        with self.driver.session() as session:
            
            root = session.read_transaction(self.__get_node, project_tree_id)
            if root['id'].startswith('NO'):
                if not root['isRoot']:
                    return None
                text = root['text']
                
            elif root['id'].startswith('NC'):
                text = root['question']
            dictionary = {'id': project_tree_id, 'text': text, 'cssClass': root['cssClass'], 'children': []}
            result = session.read_transaction(self.__get_project_tree_by_id, project_tree_id)
            
            for res in result:
                child = res.data()['child']
                if child['id'].startswith('NO'):
                    text = child['text']
                elif child['id'].startswith('NC'):
                    text = child['question']
                else:
                    break
                grand_child = self.get_project_tree_by_id(child['id'])
                if grand_child != None:
                    dictionary['children'].append(self.get_project_tree_by_id(child['id']))
            return dictionary
    
    def get_ponderations(self, carto_id):
        objects = self.get_all_cartography_child(carto_id)
        obj_weighting_dict = {}
        for obj in objects:
            with self.driver.session() as session:
                ponderations = session.read_transaction(self.__get_ponderations, obj['id'])
                if len(ponderations) != 0:
                    pond_dict = {}
                    for ponderation in ponderations:
                        ponderationDict = {}
                        ponderationDict.update({'question':ponderation['question'], 'zone':ponderation['zone'], 'nb_seconds':ponderation['nb_seconds'], 'id':ponderation['id']})
                        pond_dict={ponderation['id']:ponderationDict}
                    obj_weighting_dict.update({obj['id']:pond_dict})
        return obj_weighting_dict

    def get_all_organizations(self):
        with self.driver.session() as session:
            result = session.read_transaction(self.__get_all_organizations)
            resultArray = []
            
            for res in result:
                dictionary = {}
                dictionary['securityCode'] = res['security_code']
                dictionary['name'] = res['name']
                dictionary['id'] = res['id']
                dictionary['info'] = res['info']
                resultArray.append(dictionary)
            return resultArray

    def organization_exists(self, name: str) -> dict:
        with self.driver.session() as session:
            organization_exists = True
            result = session.read_transaction(self.__get_organization, name)
            if result == None:
                organization_exists = False

            return organization_exists       
            
    def set_state_status(self, carto_id: str, status:str) -> None:
        with self.driver.session() as session:
            result = session.write_transaction(self.__set_state_status, carto_id, status)         
            
                    
    # function to reset database       
    def clear_database(self):
        with self.driver.session() as session:
            session.write_transaction(self.__clear_database)

     
     
    
    # All queries
    # ----------------------------------------------------------------------------------  
    # create any node
    @staticmethod
    def __create_node(tx, features: dict):
        # features: dictionary where key is the name of the property and value it's value (including 'id': id field)
        id = features['id']
        # an array is required for apoc.create.node method
        node_type = [node_type_from_id[id[1]].value]
        tx.run(
            "CALL apoc.create.node($node_type, $features) "
            "YIELD node "
            "RETURN node ", node_type=node_type, features=features
        )
    
    # update all properties of a node
    @staticmethod
    def __set_node_properties(tx, info: dict):
        # info: dictionary where key is the name of the property and value it's value
        node_id = info['id']
        #node_type = node_type_from_id[node_id[1]].value
        tx.run(
            "UNWIND keys($info) AS key "
            "Match (n) "
            "WHERE n.id=$node_id "
            "WITH n, key "
            "CALL apoc.create.setProperty(n, key, $info[key]) "
            "YIELD node "
            "RETURN node ", node_id=node_id, info=info
        )
    @staticmethod
    def __delete_node(tx, node_id: str):
        query = (
            "MATCH (o) "
            "WHERE o.id = $node_id "
            "DETACH DELETE o "
        )
        tx.run(query, node_id=node_id)
    
    @staticmethod
    def __create_relation(tx, start_id: str, end_id: str, edge):
        relation_type = edge_type[edge.info['type']].value
        query = (
            "MATCH (first_node), (second_node) "
            "WHERE first_node.id = $start_id AND second_node.id = $end_id "
            "CALL apoc.create.relationship(first_node, $relation_type, $info, second_node) "
            "YIELD rel "
            "RETURN rel "
        )
        tx.run(query, start_id=start_id, end_id=end_id, relation_type=relation_type, info=edge.info)

    @staticmethod
    def __update_relation(tx, start_id: str, end_id: str, edge):
        query = (
            "MATCH (first_node)-[r]->(second_node) "
            "WHERE first_node.id = $start_id AND second_node.id = $end_id "
            "UNWIND keys($info) AS key "
            "WITH r, key "
            "CALL apoc.create.setRelProperty(r, key, $info[key]) "
            "YIELD rel "
            "RETURN rel AS rel")
        result = tx.run(query, start_id=start_id, end_id=end_id, info=edge.info)
        try:
            return result.single()['rel']
        except:
            return None 
        
    # TODO
    # put the type dynamically in case there is more than one relation between two nodes
    @staticmethod
    def __delete_relation(tx, first_id:str, second_id:str):
        query = (
            "MATCH (o1)-[r]->(o2) "
            "WHERE o1.id = $first_id AND o2.id = $second_id "
            "DELETE r "
        )
        tx.run(query, first_id=first_id, second_id=second_id)
    
    @staticmethod
    def __get_participant_by_email(tx, email:str):
        result = tx.run("MATCH (p:Participant) "
               "WHERE p.email = $email "
               "RETURN p AS participant ", email=email)
        try:
            return result.single()['participant']
        except:
            return None 

        
    # to get a specific participant
    @staticmethod
    def __get_node(tx, id: str):
        result = tx.run("MATCH (node) "
                        "WHERE node.id = $id "
                        "RETURN node AS node", id=id)
        
        try:
            return result.single()['node']
        except:
            return None
    

    # to get a specific participant
    @staticmethod
    def __get_participant(tx, email: str):
        result = tx.run("MATCH (participant: Participant) "
                        "WHERE participant.email = $email "
                        "RETURN participant AS participant", email=email)
        
        try:
            return result.single()['participant']
        except:
            return None
    
    @staticmethod
    def __get_object_in_zone(tx, carto_id:str, zone: int):
        result = tx.run("MATCH (carto: Cartography) - [r:ObjectCartographyRelation] -> (object:Object) "
                        "WHERE carto.id = $carto_id AND object.zone = $zone "
                        "RETURN object.id AS object_id ",  carto_id=carto_id, zone=zone)
        try:
            return [ i for i in result]
        except:
            return None
    #to get a specific organization
    @staticmethod
    def __get_organization(tx, name: str):
        result = tx.run("MATCH (organization: Organization) "
                        "WHERE organization.name = $name "
                        "RETURN organization AS organization", name=name)
        
        try:
            return result.single()['organization']
        except:
            return None
    
    
    
    @staticmethod
    def __get_participant_projects(tx, email:str):
        result = tx.run("MATCH (participant: Participant) - [r:ParticipantAsAccessProject] -> (c:Object) "
                        "WHERE participant.email = $email AND c.isRoot = True "
                        "RETURN c.id AS id", email=email)
        try:
            return [ i for i in result]
        except:
            return None

 
    @staticmethod
    def __get_immediate_root(tx, id_node: str):
        result = tx.run("MATCH (root: Object) - [:ObjectCartographyRelation | CartographyCartographyRelation*] -> (object) "
                        "WHERE object.id = $id AND root.isRoot = True "
                        "RETURN root AS root ", id=id_node)
        try:
            return result.single()['root']
        except:
            return None
    
    @staticmethod
    def __get_all_cartography_child(tx, cartography_id:str):
        result = tx.run("MATCH (cartography:Cartography) - [r] -> (object: Object) "
                        "WHERE cartography.id = $cartography_id "
                        "RETURN object as object ", cartography_id=cartography_id
                        )
        try:
            return [ i for i in result]
        except:
            return None
    
    @staticmethod
    def __get_ponderations(tx, object_id:str):
        result = tx.run("MATCH (object:Object) - [r] -> (weighting: Weighting) "
                        "WHERE object.id = $object_id "
                        "RETURN weighting as weighting ", object_id=object_id
                        )
        try:
            return [ i['weighting'] for i in result]
        except:
            return None
        
    @staticmethod
    def __get_all_organizations(tx):
        result = tx.run("MATCH (organization: Organization) "
                        "RETURN organization"
                        )
        try:
            return [ i['organization'] for i in result]
        except:
            return None
    
    # AND child.isRoot = True
    @staticmethod
    def __get_project_tree_by_id(tx, project_id: str):
        
        result = tx.run("MATCH (root) - [r] -> (child) "
                        "WHERE root.id = $project_id  "
                        "RETURN child AS child ", project_id=project_id
                        )
        try:
            return [ i for i in result]
        except:
            return None
    
    
    @staticmethod
    def __set_state_status(tx, id_carto:str, status:str):
        result = tx.run("MATCH (carto: Cartography) "
                        "WHERE carto.id = $id_carto "
                        "SET carto.status = $status "
                        "RETURN carto AS carto ", id_carto=id_carto, status=status)
  
       
    
    # to get all next id
    @staticmethod
    def __get_next_id_node(tx):
        result = tx.run("MATCH (id_node: IDGenerator) "
                        "RETURN id_node AS id ")
        return result.single()['id']
    


    @staticmethod
    def __set_next_id_node(tx, id_generator):
    # id_generator : object of IDGenerator class
        tx.run("MATCH (id_node: IDGenerator) "
               "SET id_node.next_object_id = $next_object_id "
               "SET id_node.next_participant_id = $next_participant_id "
               "SET id_node.next_cartography_id = $next_cartography_id "
               "SET id_node.next_weighting_id = $next_weighting_id "
               "SET id_node.next_organization_id = $next_organization_id "
               "RETURN id_node.id ", next_object_id=id_generator.get_object_id(), 
               next_participant_id=id_generator.get_participant_id(), 
               next_cartography_id=id_generator.get_cartography_id(), 
               next_weighting_id=id_generator.get_weighting_id(), 
               next_organization_id=id_generator.get_organization_id())
 
    # to clear the database
    @staticmethod
    def __clear_database(tx):  
        result = tx.run("MATCH (a) - [r] -> (b) "
                        "DELETE r "
                        "RETURN r ")
        result = tx.run("MATCH (a) "
                        "DELETE a "
                        "RETURN a ")
        result = tx.run("CREATE (id:IDGenerator) "
                        "SET id.next_object_id = 'NO0' "
                        "SET id.next_participant_id = 'NP0' "
                        "SET id.next_cartography_id = 'NC0' "
                        "SET id.next_weighting_id = 'NW0' "
                        "SET id.next_organization_id = 'NG0' "
                        "RETURN id ")
    
    # ----------------------------------------------------------------------------------

    
