import { IOrgan } from 'src/app/interfaces/zone/zone-path.interface';

export const ORGANS: IOrgan[] = [
  {
    index: 1,
    zones: [1, 9, 17],
  },
  {
    index: 2,
    zones: [2, 10, 18],
  },
  {
    index: 3,
    zones: [3, 11, 19],
  },
  {
    index: 4,
    zones: [4, 12, 20],
  },
  {
    index: 5,
    zones: [5, 13, 21],
  },
  {
    index: 6,
    zones: [6, 14, 22],
  },
  {
    index: 7,
    zones: [7, 15, 23],
  },
  {
    index: 8,
    zones: [8, 16, 24],
  },
];
