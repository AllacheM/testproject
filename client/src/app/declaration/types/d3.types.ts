import { BaseType, Selection } from 'd3';

export type d3SVG = Selection<SVGGElement, unknown, HTMLElement, any>;
export type d3G = Selection<BaseType, unknown, HTMLElement, any>;
