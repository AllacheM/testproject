export const MATRIX_ROOM_EVENT_NAME = 'joinMatrixRoom';
export const MATRIX_DISCONNECTION_EVENT_NAME = 'matrixDisconnect';
export const MATRIX_LEAVE_ROOM_EVENT_NAME = 'leaveMatrixRoom';

// postit events
export const NEW_POSTIT_EVENT = 'newPostItCreated';
export const DRAGGING_POSTIT_EVENT = 'draggingPostIt';
export const DRAG_START_POSTIT_EVENT = 'dragStartPostIt';
export const DRAG_END_POSTIT_EVENT = 'dragEndPostIt';
