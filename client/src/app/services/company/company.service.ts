import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IHttpResponse } from 'src/app/interfaces/http.interface';
import { HttpService } from '../http/http.service';
import { UserService } from '../user/user.service';
import { Observable } from 'rxjs';
import { ICompany } from 'src/app/interfaces/company.interface';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  private allCompanies: ICompany[] = [];
  constructor(private userService: UserService, private httpService: HttpService) { }

  createCompany(name: string, info: string): Observable<HttpResponse<IHttpResponse<any>>> {
    const formData = new FormData();
    formData.append('email', this.userService.email)
    formData.append('name', name);
    formData.append('info', info);

    return this.httpService.post<FormData, IHttpResponse<any>>(
      'create_company',
      formData
    );
  }

  onSuccessfulCompanyCreation(data: IHttpResponse<any>){
    this.allCompanies.push({id: data.data.id, name: data.data.name, info: data.data.info, securityCode: data.data.securityCode})
  }

  getCompanies(): Observable<HttpResponse<IHttpResponse<any>>> {

    return this.httpService.get<IHttpResponse<any>>(
      'get_all_companies');
  }

  addCompanies(data: IHttpResponse<any>){
    for(let company of data.data.companies){
      this.allCompanies.push({id: company.id, name: company.name, info: company.info, securityCode: company.securityCode})

    }
  }
  clearCompanies(){
    this.allCompanies = [];
  }
  get companies(): ICompany[]{
    return this.allCompanies;
  }
}
