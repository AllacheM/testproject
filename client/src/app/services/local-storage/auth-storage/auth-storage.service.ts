import { Injectable } from '@angular/core';
import { AbsLocalStorage } from 'src/app/classes/abs-local-storage';

@Injectable({
  providedIn: 'root',
})
export class AuthStorageService extends AbsLocalStorage {
  constructor() {
    super('cibleExpert_userToken');
  }
}
