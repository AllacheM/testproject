import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IHttpResponse } from 'src/app/interfaces/http.interface';
import { IUser } from 'src/app/interfaces/user.interface';
import { HttpService } from '../http/http.service';
import { AuthStorageService } from '../local-storage/auth-storage/auth-storage.service';
import { RouterService } from '../router/router.service';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private authStorageService: AuthStorageService,
    private httpService: HttpService,
    private routerService: RouterService,
    private userService: UserService
  ) {}

  async isAuthenticated(): Promise<boolean> {
    return new Promise<boolean>(async (resolve, reject) => {
      const token = this.authStorageService.get();

      if (!token) return resolve(false);

      const formData = new FormData();
      formData.append('jwt', token);

      this.httpService
        .post<FormData, IHttpResponse<any>>('get_user_by_token', formData)
        .subscribe({
          next: (response: HttpResponse<IHttpResponse<any>>) => {
            const data = response.body;

            if (data?.code !== 1) {
              return resolve(false);
            }

            this.userService.setUser(JSON.parse(data.data.user) as IUser); // TODO change true to real thing for admin
            return resolve(true);
          },
          error: (error: HttpErrorResponse) => {
            return resolve(false);
          },
        });
    });
  }

  isAdmin(): boolean {
    return this.userService.isAdmin;
  }

  login(
    email: string,
    password: string
  ): Observable<HttpResponse<IHttpResponse<any>>> {
    const formData = new FormData();
    formData.append('email', email);
    formData.append('password', password);

    return this.httpService.post<FormData, IHttpResponse<any>>(
      'authenticate_user',
      formData
    );
  }

  createUser(
    user: string,
    email: string,
    password: string
  ): Observable<HttpResponse<IHttpResponse<any>>> {
    const formData = new FormData();
    formData.append('user', user);
    formData.append('email', email);
    formData.append('password', password);

    return this.httpService.post<FormData, IHttpResponse<any>>(
      'create_user',
      formData
    );
  }

  onLoginSuccessful(data: IHttpResponse<any>): void {
    this.authStorageService.set(data.data.jwt);
    this.userService.setUser(JSON.parse(data.data.user) as IUser); // TODO change true to real thing for admin
    this.routerService.navigateToHome();
  }

  logOut(): void {
    const formData = new FormData();
    formData.append('jwt', this.authStorageService.get()!);

    this.httpService
      .post<FormData, IHttpResponse<any>>('disconnect', formData)
      .subscribe({
        next: () => this.onLogoutCallback(),
        error: () => this.onLogoutCallback(),
      });
  }

  private onLogoutCallback() {
    this.userService.setUser(null);
    this.authStorageService.remove();
    this.routerService.navigateToLogin();
  }
}
