import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import * as d3 from 'd3';
import { D3Tree } from 'src/app/classes/tree/d3-tree.class';
import { ITreeData, ITreeNode } from 'src/app/interfaces/d3-tree.interface';
import { RouterService } from '../../router/router.service';
import { HttpService } from '../../http/http.service';
import { IHttpResponse } from 'src/app/interfaces/http.interface';
import { HttpResponse } from '@angular/common/http';
import { AuthStorageService } from '../../local-storage/auth-storage/auth-storage.service';

@Injectable({
  providedIn: 'root',
})
export class ProjectTreeService {
  private projectTrees: ITreeData[] = [];
  private subjectProjectTrees: Subject<ITreeData[]>;
  private onNodeClickedCb: (e: PointerEvent, d: ITreeNode) => void;

  constructor(
    private routerService: RouterService,
    private httpService: HttpService,
    private authStorageService: AuthStorageService
  ) {
    this.subjectProjectTrees = new Subject();
    this.onNodeClickedCb = this.onNodeClicked.bind(this);
  }

  fetchProjectTrees(): void {
    this.httpService
      .get<IHttpResponse<ITreeData[]>>(
        `get_project_trees/${this.authStorageService.get()!}`
      )
      .subscribe({
        next: (data: HttpResponse<IHttpResponse<ITreeData[]>>) => {
          this.projectTrees = data.body?.data as ITreeData[];
          this.subjectProjectTrees.next(this.projectTrees);
        },
      });
  }

  get subjectProjectTreesObs(): Observable<ITreeData[]> {
    return this.subjectProjectTrees.asObservable();
  }

  addProjectTree(projectTree: ITreeData) {
    this.projectTrees.push(projectTree);
    this.createTree(projectTree);
  }

  drawTrees(): void {
    if (this.projectTrees.length === 0) return;

    this.projectTrees.forEach((tree) => this.createTree(tree));
  }

  onNodeClicked(e: PointerEvent, node: ITreeNode): void {
    d3.selectAll(`.project-tree`).remove();
    this.routerService.navigateToBoard(node.data.id);
  }

  createTree(treeData: ITreeData): void {
    const projectTree = d3
      .select('#projects-container')
      .append('div')
      .attr('class', 'project-tree');

    const width = parseInt(projectTree.style('width'));
    const height = parseInt(projectTree.style('height'));

    const svgContainer = projectTree
      .append('svg')
      .attr('id', `svg-project-tree-${treeData.id}`)
      .attr('width', width)
      .attr('height', height);

    const svg = svgContainer.append('g');

    new D3Tree(treeData, svg, width, height, this.onNodeClickedCb);

    svgContainer.call(
      d3.zoom().on('zoom', function (event: any) {
        svg.attr('transform', event.transform);
      }) as any
    );
  }
}
