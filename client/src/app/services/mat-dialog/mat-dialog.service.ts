import { Injectable } from '@angular/core';
import { ComponentType } from '@angular/cdk/portal';
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root',
})
export class MatDialogService {
  private readonly dialogOptions = {
    autoFocus: true,
  };

  constructor(public dialog: MatDialog) {}

  private getDialogOptionsWithData(data: unknown) {
    return { ...this.dialogOptions, data };
  }

  open(component: ComponentType<unknown>) {
    this.dialog.open(component, this.dialogOptions);
  }

  openWithData(component: ComponentType<unknown>, data: unknown) {
    this.dialog.open(component, this.getDialogOptionsWithData(data));
  }
}
