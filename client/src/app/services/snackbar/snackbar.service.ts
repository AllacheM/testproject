import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class SnackbarService {
  constructor(private _snackBar: MatSnackBar) {}

  showSuccessWithoutAction(message: string) {
    this._snackBar.open(message, undefined, {
      panelClass: ['success-container', 'center-snackbar-text'],
    });
  }

  showErrorWithoutAction(message: string) {
    this._snackBar.open(message, undefined, {
      panelClass: ['warning-container', 'center-snackbar-text'],
    });
  }
}
