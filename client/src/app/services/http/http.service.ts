import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { AppModule } from 'src/app/modules/app/app.module';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  protected endpointUrl: string = environment.serverURL;
  protected readonly httpClient: HttpClient;

  constructor() {
    this.httpClient = AppModule.injector.get(HttpClient);
  }

  private getEndpoint(endpoint: string): string {
    return `${this.endpointUrl}/${endpoint}`;
  }

  setEndpoint(endpoint: string) {
    if (endpoint) {
      this.endpointUrl += `/${endpoint}`;
    }
  }

  get<T>(endpoint: string): Observable<HttpResponse<T>> {
    return this.httpClient
      .get<T>(this.getEndpoint(endpoint), {
        observe: 'response',
        withCredentials: true,
      })
      .pipe(
        map((responseData: HttpResponse<T>) => {
          return responseData;
        })
      );
  }

  post<T, G>(
    endpoint: string,
    data: T,
    headers?: HttpHeaders
  ): Observable<HttpResponse<G>> {
    return this.httpClient
      .post<G>(this.getEndpoint(endpoint), data, {
        headers,
        observe: 'response',
        withCredentials: true,
      })
      .pipe(
        map((responseData: HttpResponse<G>) => {
          return responseData;
        })
      );
  }

  patch<T, G>(
    endpoint: string,
    data: T,
    headers?: HttpHeaders
  ): Observable<HttpResponse<G>> {
    return this.httpClient
      .patch<G>(this.getEndpoint(endpoint), data, {
        headers,
        observe: 'response',
        withCredentials: true,
      })
      .pipe(
        map((responseData: HttpResponse<G>) => {
          return responseData;
        })
      );
  }

  delete<G>(
    endpoint: string,
    headers?: HttpHeaders
  ): Observable<HttpResponse<G>> {
    return this.httpClient
      .delete<G>(this.getEndpoint(endpoint), {
        headers,
        observe: 'response',
        withCredentials: true,
      })
      .pipe(
        map((responseData: HttpResponse<G>) => {
          return responseData;
        })
      );
  }
}
