import { Injectable } from '@angular/core';
import { AbsHttpService } from 'src/app/classes/abs-http';

@Injectable({
  providedIn: 'root',
})
export class AuthHttpService extends AbsHttpService {
  constructor() {
    super('auth');
  }
}
