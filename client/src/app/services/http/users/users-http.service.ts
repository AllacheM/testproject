import { Injectable } from '@angular/core';
import { HttpService } from '../http.service';

@Injectable({
  providedIn: 'root',
})
export class UsersHttpService extends HttpService {
  constructor() {
    super();
    this.setEndpoint('users'); // /api/users/*
  }
}
