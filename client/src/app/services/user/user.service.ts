import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IHttpResponse } from 'src/app/interfaces/http.interface';
import { IUser } from 'src/app/interfaces/user.interface';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private user!: IUser | null;

  constructor(private httpService: HttpService) {}

  get isAdmin(): boolean {
    return this.user?.role === 'admin';
  }

  get username(): string {
    return this.user?.name!;
  }

  get email(): string {
    return this.user?.email!;
  }

  setUser(user: IUser | null) {
    this.user = user;

    // to remove
    if (!user) return;

    if (user.email === 'admin@admin.com') {
      this.user!.role = 'admin';
    }
  }

  changePassword(
    oldPwd: string,
    newPwd: string
  ): Observable<HttpResponse<IHttpResponse<any>>> {
    const formData = new FormData();
    formData.append('email', this.user!.email);
    formData.append('password', oldPwd);
    formData.append('newpassword', newPwd);
    console.log('', formData);

    return this.httpService.post<FormData, IHttpResponse<any>>(
      'change_password',
      formData
    );
  }
}
