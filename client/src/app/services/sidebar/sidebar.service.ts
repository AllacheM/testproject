import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SidebarService {
  private isSidebarOpen: boolean;
  private isSidebarOpenSubject: Subject<boolean>;

  constructor() {
    this.isSidebarOpen = false;
    this.isSidebarOpenSubject = new Subject();
  }

  toggle(): void {
    this.isSidebarOpen = !this.isSidebarOpen;
    this.isSidebarOpenSubject.next(this.isSidebarOpen);
  }

  getObservable(): Observable<boolean> {
    return this.isSidebarOpenSubject.asObservable();
  }
}
