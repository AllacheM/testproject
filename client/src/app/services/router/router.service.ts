import { Injectable } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class RouterService {
  constructor(protected router: Router, private route: ActivatedRoute) {}

  private navigateTo(url: string): void {
    this.router.navigateByUrl(url);
  }

  isHome(): boolean {
    return this.router.url === '/';
  }

  navigateToLogin() {
    this.navigateTo('/auth/login');
  }

  navigateToSignUp() {
    this.navigateTo('/auth/signUp');
  }

  navigateToProfile() {
    this.navigateTo('/profile');
  }

  navigateToAdmin() {
    this.navigateTo('/admin');
  }

  navigateToHome() {
    this.navigateTo('/');
  }

  navigateToBoard(id: number) {
    this.navigateTo(`/board/${id}`);
  }

  addQueryParamsToUrl(params: Params) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { ...params },
    });
  }
}
