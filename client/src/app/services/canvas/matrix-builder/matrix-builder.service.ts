import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import { D3Utils } from 'src/app/classes/utils/d3.util';
import { ZoneUtils, ZONE_CLASSES } from 'src/app/classes/utils/zone.util';
import { d3G } from 'src/app/declaration/types/d3.types';
import {
  IZone,
  IZoneCoords,
} from 'src/app/interfaces/zone/zone-path.interface';
import { CanvasBuilderService } from '../canvas-builder/canvas-builder.service';
import { AbsMatrixStateService } from '../matrix-state/abstract-matrix-state/abs-matrix-state.service';
import { MatrixStateService } from '../matrix-state/matrix-state-service/matrix-state.service';
import { ProjectMatrixService } from '../project-matrix/project-matrix.service';
import { ZoneStrategyService } from '../zone-strategy/zone-strategy.service';

@Injectable({
  providedIn: 'root',
})
export class MatrixBuilderService {
  constructor(
    private canvasBuilderService: CanvasBuilderService,
    private projectMatrixService: ProjectMatrixService,
    private zoneStrategyService: ZoneStrategyService,
    private matrixStateService: MatrixStateService
  ) {}

  build() {
    this.drawAreas();
    this.drawLines();
    this.setFunctionText();
  }

  buildWithoutFunction() {
    this.drawAreas();
    this.drawLines();
  }

  getGZone(index: number): d3G {
    return this.canvasBuilderService
      .getSvg()
      .select(`#${this.getZoneId(index)}`);
  }

  private get state(): AbsMatrixStateService {
    return this.matrixStateService.getState();
  }

  private getZoneId(id: number): string {
    return `zone-${id}`;
  }

  private calculateZoneArea = () => {
    return d3
      .area<IZoneCoords>()
      .x0((d: IZoneCoords) => d.x0)
      .x1((d: IZoneCoords) => d.x1)
      .y0((d: IZoneCoords) => d.y0)
      .y1((d: IZoneCoords) => d.y1);
  };

  private drawAreas(): void {
    this.canvasBuilderService
      .getSvg()
      .selectAll('path')
      .data(this.zoneStrategyService.areaData)
      .enter()
      .append('g')
      .attr('class', ZONE_CLASSES.areaZone)
      .attr('id', (d) => this.getZoneId(d.index))
      .attr(ZONE_CLASSES.areaZoneId, (d) => d.index)
      .attr('cursor', this.state.getAreaPointer())
      .each((zone) => {
        this.drawZone(zone);
        this.drawZoneText(zone);
      })
      .on('click', (event: PointerEvent, zone: IZone) => {
        this.state.onZoneClicked(event, zone);
      })
      .on('mouseover', (event: PointerEvent, zone: IZone) => {
        this.state.onMouseOverZone(event, zone);
      })
      .on('mouseenter', (event: PointerEvent, zone: IZone) => {
        this.state.onMouseEnterZone(event, zone);
      })
      .on('mouseleave', (event: PointerEvent, zone: IZone) => {
        this.state.onMouseLeaveZone(event, zone);
      });
  }

  private drawLines(): void {
    this.canvasBuilderService
      .getSvg()
      .selectAll('matrix-lines')
      .data(this.zoneStrategyService.lineData)
      .enter()
      .append('line')
      .attr('x1', (d) => d.x0)
      .attr('y1', (d) => d.y0)
      .attr('x2', (d) => d.x1)
      .attr('y2', (d) => d.y1)
      .style('stroke', '#bbb')
      .style('stroke-width', this.state.getLineStrokeWidth());
  }

  private drawZone(zone: IZone): void {
    this.getGZone(zone.index)
      .selectAll('path')
      .data(zone.path)
      .enter()
      .append('path')
      .attr('class', ZONE_CLASSES.areaZonePath)
      .attr('d', this.calculateZoneArea())
      .attr('fill', zone.color);
  }

  private drawZoneText(zone: IZone): void {
    const g = this.getGZone(zone.index);
    const gPosition = (g.node() as SVGGraphicsElement).getBBox();
    const [x, y, textAnchor] = ZoneUtils.getTextPosition(
      gPosition,
      zone.text.direction
    );

    g.append('text')
      .attr('class', ZONE_CLASSES.areaZoneName)
      .text(`${zone.text.name} (#${zone.index})`)
      .attr('x', x)
      .attr('y', y)
      .attr('font-size', '10px')
      .attr('text-anchor', textAnchor);
  }

  private setFunctionText() {
    this.getGZone(this.zoneStrategyService.zoneFunction.index!)
      .select(`.${ZONE_CLASSES.areaZoneName}`)
      .attr('font-weight', 'bold')
      .attr('font-size', '2em')
      .attr(
        'transform',
        `translate(0, ${this.zoneStrategyService.zoneHeightSize / 2})`
      )
      .text(this.projectMatrixService.cartographyQuestion)
      .call(D3Utils.wrapText, this.zoneStrategyService.zoneWidthSize);
  }
}
