import { TestBed } from '@angular/core/testing';
import { MatrixBuilderService } from './matrix-builder.service';

describe('MatrixBuilderService', () => {
  let service: MatrixBuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatrixBuilderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
