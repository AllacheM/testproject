import { Injectable } from '@angular/core';
import ContextMenuFactory from 'd3-context-menu';
import { IContextMenu } from 'src/app/interfaces/matrix/context-menu.interface';
import { IPostit } from 'src/app/interfaces/matrix/project-matrix.interface';
import { RouterService } from '../../router/router.service';
import { MatrixDrawingSocketService } from '../../socket/matrix-drawing-socket/matrix-drawing-socket.service';

// https://github.com/patorjk/d3-context-menu

@Injectable({
  providedIn: 'root',
})
export class ContextMenuService {
  constructor(
    private routerService: RouterService,
    private matrixDrawingSocketService: MatrixDrawingSocketService
  ) {}

  get postitBrainstormingContextMenu() {
    return ContextMenuFactory((data: IPostit) =>
      this.getPostitBrainstormingMenu(data, this)
    );
  }

  get postitPonderatingContextMenu() {
    return ContextMenuFactory(this.getPostitPonderatingMenu);
  }

  private getPostitBrainstormingMenu(
    postit: IPostit,
    thisService: ContextMenuService
  ): IContextMenu<IPostit>[] {
    const openBoardMenu: IContextMenu<IPostit> = {
      title: 'Ouvrir espace',
      action: (data: IPostit, event: PointerEvent) => {
        this.matrixDrawingSocketService.destroy();
        thisService.routerService.navigateToBoard(data.id);
      },
    };

    return postit.isRoot ? [openBoardMenu] : [];
  }

  private getPostitPonderatingMenu(): IContextMenu<IPostit>[] {
    return [
      {
        title: 'TESST PONDERATION',
        action: (data: IPostit, event: PointerEvent) => {},
      },
      {
        divider: true,
      },
      {
        title: 'WORKS',
      },
    ];
  }
}
