import { TestBed } from '@angular/core/testing';

import { AbsMatrixStateService } from './abs-matrix-state.service';

describe('AbsMatrixStateService', () => {
  let service: AbsMatrixStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AbsMatrixStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
