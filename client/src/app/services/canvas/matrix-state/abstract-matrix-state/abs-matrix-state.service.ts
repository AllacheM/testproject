import { IOrgan, IZone } from 'src/app/interfaces/zone/zone-path.interface';
import { AppModule } from 'src/app/modules/app/app.module';
import { ZoneStrategyService } from '../../zone-strategy/zone-strategy.service';
import * as d3 from 'd3';
import { Observable, Subject } from 'rxjs';
import { ContextMenuService } from '../../context-menu/context-menu.service';
import { RouterService } from 'src/app/services/router/router.service';

export abstract class AbsMatrixStateService {
  protected zoneStrategyService: ZoneStrategyService;
  protected contextMenuService: ContextMenuService;
  protected routerService: RouterService;

  protected mouseOverAreaSubject: Subject<Partial<IZone>>;

  constructor() {
    this.zoneStrategyService = AppModule.injector.get(ZoneStrategyService);
    this.contextMenuService = AppModule.injector.get(ContextMenuService);
    this.routerService = AppModule.injector.get(RouterService);
    this.mouseOverAreaSubject = new Subject();
  }

  abstract onZoneClicked(event: PointerEvent, zone: IZone): void;
  abstract onMouseOverZone(event: PointerEvent, zone: IZone): void;
  abstract onMouseEnterZone(event: PointerEvent, zone: IZone): void;
  abstract onMouseLeaveZone(event: PointerEvent, zone: IZone): void;
  abstract getAreaPointer(): string;
  abstract getZonePointer(): string;
  abstract get contextMenu(): any;

  protected getOrgan(zoneIndex: number): IOrgan {
    return this.zoneStrategyService.getOrgan(zoneIndex);
  }

  public get mouseOverAreaObservable(): Observable<Partial<IZone>> {
    return this.mouseOverAreaSubject.asObservable();
  }

  getLineStrokeWidth(): number {
    return 1;
  }

  protected changeZoneBrightness(
    organ: IOrgan,
    selector: string,
    brightness: number
  ): void {
    organ.zones.forEach((zoneIndex) => {
      d3.select(selector + zoneIndex).attr(
        'filter',
        `brightness(${brightness}%)`
      );
    });
  }
}
