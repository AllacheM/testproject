import { Injectable } from '@angular/core';
import { AbsMatrixStateService } from '../abstract-matrix-state/abs-matrix-state.service';
import { ThreeStateBrainstormingService } from '../states/brainstorming-state/three-state/three-state-brainstorming.service';
import { TwentySevenBrainstormingService } from '../states/brainstorming-state/twenty-seven-state/twenty-seven-brainstorming.service';
import { PonderationStateService } from '../states/ponderation-state/ponderation-state.service';

export enum MatrixStates {
  brainstorming_27 = '27b',
  brainstorming_3 = '3b',
  ponderation = 'p',
}

@Injectable({
  providedIn: 'root',
})
export class MatrixStateService {
  private stateService!: AbsMatrixStateService;

  constructor(
    private twentySevenBrainstormingService: TwentySevenBrainstormingService,
    private threeStateBrainstormingService: ThreeStateBrainstormingService,
    private ponderationStateService: PonderationStateService
  ) {}

  setState(type: MatrixStates): void {
    this.stateService = this.getStateFactory(type);
  }

  getState(): AbsMatrixStateService {
    return this.stateService;
  }

  getStateFactory(type: MatrixStates): AbsMatrixStateService {
    switch (type) {
      case MatrixStates.brainstorming_27:
        return this.twentySevenBrainstormingService;
      case MatrixStates.brainstorming_3:
        return this.threeStateBrainstormingService;
      case MatrixStates.ponderation:
        return this.ponderationStateService;
    }
  }
}
