import { TestBed } from '@angular/core/testing';

import { MatrixStateService } from './matrix-state.service';

describe('MatrixStateService', () => {
  let service: MatrixStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatrixStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
