import { TestBed } from '@angular/core/testing';

import { TwentySevenBrainstormingService } from './twenty-seven-brainstorming.service';

describe('TwentySevenBrainstormingService', () => {
  let service: TwentySevenBrainstormingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TwentySevenBrainstormingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
