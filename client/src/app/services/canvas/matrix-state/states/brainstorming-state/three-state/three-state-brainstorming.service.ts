import { Injectable } from '@angular/core';
import { IZone } from 'src/app/interfaces/zone/zone-path.interface';
import { BrainstormingStateService } from '../index/brainstorming-state.service';

@Injectable({
  providedIn: 'root',
})
export class ThreeStateBrainstormingService extends BrainstormingStateService {
  constructor() {
    super();
  }

  getAreaPointer(): string {
    return 'initial';
  }

  override getLineStrokeWidth(): number {
    return 3;
  }

  onMouseEnter(event: PointerEvent, zone: IZone) {}

  onMouseLeave(event: PointerEvent, zone: IZone): void {}
}
