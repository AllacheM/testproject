import { Injectable } from '@angular/core';
import { IZone } from 'src/app/interfaces/zone/zone-path.interface';
import { AbsMatrixStateService } from '../../../abstract-matrix-state/abs-matrix-state.service';

@Injectable({
  providedIn: 'root',
})
export abstract class BrainstormingStateService extends AbsMatrixStateService {
  constructor() {
    super();
  }

  getZonePointer(): string {
    return 'initial';
  }

  get contextMenu() {
    return this.contextMenuService.postitBrainstormingContextMenu;
  }

  onZoneClicked(event: PointerEvent, zone: IZone): void {
    const { index } = this.getOrgan(zone.index);
    this.routerService.addQueryParamsToUrl({ organIndex: index });
  }

  onMouseOverZone(event: PointerEvent, zone: IZone): void {
    this.mouseOverAreaSubject.next(zone);
  }

  onMouseEnterZone(event: PointerEvent, zone: IZone): void {
    this.onMouseEnter(event, zone);
  }

  onMouseLeaveZone(event: PointerEvent, zone: IZone): void {
    this.onMouseLeave(event, zone);
  }

  abstract onMouseEnter(event: PointerEvent, zone: IZone): void;
  abstract onMouseLeave(event: PointerEvent, zone: IZone): void;
}
