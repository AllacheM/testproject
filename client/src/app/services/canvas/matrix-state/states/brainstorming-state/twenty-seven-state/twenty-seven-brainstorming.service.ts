import { Injectable } from '@angular/core';
import { IZone } from 'src/app/interfaces/zone/zone-path.interface';
import { BrainstormingStateService } from '../index/brainstorming-state.service';

@Injectable({
  providedIn: 'root',
})
export class TwentySevenBrainstormingService extends BrainstormingStateService {
  constructor() {
    super();
  }

  getAreaPointer(): string {
    return 'pointer';
  }

  onMouseEnter(event: PointerEvent, zone: IZone) {
    if (!zone.isFunction) {
      this.changeZoneBrightness(this.getOrgan(zone.index), '#zone-', 105);
    }
  }

  onMouseLeave(event: PointerEvent, zone: IZone): void {
    if (!zone.isFunction) {
      this.changeZoneBrightness(this.getOrgan(zone.index), '#zone-', 100);
    }
  }
}
