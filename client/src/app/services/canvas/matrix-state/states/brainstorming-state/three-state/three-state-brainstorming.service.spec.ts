import { TestBed } from '@angular/core/testing';

import { ThreeStateBrainstormingService } from './three-state-brainstorming.service';

describe('ThreeStateBrainstormingService', () => {
  let service: ThreeStateBrainstormingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ThreeStateBrainstormingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
