import { TestBed } from '@angular/core/testing';

import { BrainstormingStateService } from './brainstorming-state.service';

describe('BrainstormingStateService', () => {
  let service: BrainstormingStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BrainstormingStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
