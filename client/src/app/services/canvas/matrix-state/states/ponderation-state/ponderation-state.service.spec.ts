import { TestBed } from '@angular/core/testing';
import { PonderationStateService } from './ponderation-state.service';

describe('PonderationStateService', () => {
  let service: PonderationStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PonderationStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
