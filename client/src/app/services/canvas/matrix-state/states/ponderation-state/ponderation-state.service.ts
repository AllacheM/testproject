import { Injectable } from '@angular/core';
import { IZone } from 'src/app/interfaces/zone/zone-path.interface';
import { AbsMatrixStateService } from '../../abstract-matrix-state/abs-matrix-state.service';

@Injectable({
  providedIn: 'root',
})
export class PonderationStateService extends AbsMatrixStateService {
  constructor() {
    super();
  }

  getAreaPointer(): string {
    return 'initial';
  }

  getZonePointer(): string {
    return 'pointer';
  }

  get contextMenu() {
    return this.contextMenuService.postitPonderatingContextMenu;
  }

  onZoneClicked(event: PointerEvent, zone: IZone): void {}

  onMouseOverZone(event: PointerEvent, zone: IZone): void {}

  onMouseEnterZone(event: PointerEvent, zone: IZone): void {
    if (!zone.isFunction) {
      // this.changeZoneBrightness(this.getOrgan(zone.index), '#zone-', 205);
    }
  }

  onMouseLeaveZone(event: PointerEvent, zone: IZone): void {}
}
