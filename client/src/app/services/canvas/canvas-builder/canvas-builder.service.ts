import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import { ZoomBehavior } from 'd3';
import { BehaviorSubject, Observable } from 'rxjs';
import { CANVAS_CLASSES } from 'src/app/classes/utils/zone.util';
import { d3SVG } from 'src/app/declaration/types/d3.types';

@Injectable({
  providedIn: 'root',
})
export class CanvasBuilderService {
  private svg!: d3SVG;
  private svgWidth!: number;
  private svgHeight!: number;
  private zoom!: ZoomBehavior<Element, unknown>;
  private canvasZoomSubject = new BehaviorSubject<number>(0);

  private readonly maxScale = 5;
  private readonly heightRatio = 0.7;
  private readonly defaultZoom = 0.1;
  private readonly maxTranslateOffset = 300;

  constructor() {
    this.svgWidth = 8000;
    this.svgHeight = this.svgWidth * this.heightRatio;

    this.initZoomBehavior();
  }

  private initZoomBehavior(): void {
    this.zoom = d3
      .zoom()
      .scaleExtent([this.defaultZoom, this.maxScale])
      .translateExtent([
        [-this.maxTranslateOffset, -this.maxTranslateOffset],
        [
          this.svgWidth + this.maxTranslateOffset,
          this.svgHeight + this.maxTranslateOffset,
        ],
      ]);
  }

  removeCanvas(): void {
    d3.select(`.${CANVAS_CLASSES.mainSvg}`).remove();
  }

  initCanvas(): void {
    const svgContainer = d3.select('#svg-canvas-container');
    const containerWidth = parseInt(svgContainer.style('width'));
    const constainerHeight = parseInt(svgContainer.style('height'));

    const defaultX =
      containerWidth / 2 - (this.svgWidth / 2) * this.defaultZoom;
    const defaultY =
      constainerHeight / 2 - (this.svgHeight / 2) * this.defaultZoom;

    this.svg = svgContainer
      .append('svg')
      .attr('class', CANVAS_CLASSES.mainSvg)
      .attr('width', containerWidth)
      .attr('height', constainerHeight)
      .call(
        this.zoom.transform as any,
        d3.zoomIdentity.translate(defaultX, defaultY).scale(this.defaultZoom)
      )
      .call(this.onZoomed())
      .append('g')
      .attr(
        'transform',
        `translate(${defaultX}  , ${defaultY}) scale(${this.defaultZoom})`
      );
  }

  private onZoomed() {
    return this.zoom.on('zoom', (event) => {
      this.svg.attr('transform', event.transform);
      this.calculateZoomPercentage(event.transform.k);
    }) as any;
  }

  getSvg(): d3SVG {
    return this.svg;
  }

  get width(): number {
    return this.svgWidth;
  }

  get height(): number {
    return this.svgHeight;
  }

  getCanvasZoomSubject(): Observable<number> {
    return this.canvasZoomSubject.asObservable();
  }

  private calculateZoomPercentage(scaleFactor: number): void {
    let scale = (scaleFactor / this.maxScale) * 100;
    scale = parseFloat(scale.toFixed());
    this.canvasZoomSubject.next(scale);
  }
}
