import { TestBed } from '@angular/core/testing';

import { ZoneStrategyService } from './zone-strategy.service';

describe('ZoneStrategyService', () => {
  let service: ZoneStrategyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ZoneStrategyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
