import { Injectable } from '@angular/core';
import { ZONE_CLASSES } from 'src/app/classes/utils/zone.util';
import {
  IOrgan,
  IZone,
  IZoneCoords,
} from 'src/app/interfaces/zone/zone-path.interface';
import { IZoneStrategy } from 'src/app/interfaces/zone/zone-strategy.interface';
import * as d3 from 'd3';
import { ORGANS } from 'src/app/declaration/matrix/organs';

@Injectable({
  providedIn: 'root',
})
export class ZoneStrategyService {
  private zoneStrategy!: IZoneStrategy;
  private zonePositionsMap!: Map<number, DOMRect>;

  constructor() {
    this.resetMap();
  }

  setZoneStrategy(zoneStrat: IZoneStrategy) {
    this.zoneStrategy = zoneStrat;
  }

  isOrganValid(index: number): boolean {
    return ORGANS.find((org) => org.index === index) !== (null || undefined);
  }

  isZoneInOrgan(zoneNb: number): boolean {
    return this.zoneStrategy
      .getOrgans()
      .some((organ) => organ.zones.includes(zoneNb));
  }

  get zoneHeightSize(): number {
    return this.zoneStrategy.getZoneHeightSize();
  }

  get zoneWidthSize(): number {
    return this.zoneStrategy.getZoneWidthSize();
  }

  get areaData(): IZone[] {
    return this.zoneStrategy.getAreaData();
  }

  get areaDataWithoutFunction(): IZone[] {
    return this.areaData.filter((zone) => !zone.isFunction);
  }

  get lineData(): IZoneCoords[] {
    return this.zoneStrategy.getLineData();
  }

  get zoneFunction(): IZone {
    return this.areaData.find((zone) => zone.isFunction) as IZone;
  }

  resetMap(): void {
    this.zonePositionsMap = new Map();
  }

  getOrgan(zoneIndex: number): IOrgan {
    return this.zoneStrategy
      .getOrgans()
      .find((organ) => organ.zones.includes(zoneIndex))!;
  }

  getZonePosition(zoneIndex: number): DOMRect {
    if (this.zonePositionsMap.has(zoneIndex)) {
      return this.zonePositionsMap.get(zoneIndex)!;
    }

    const domRect = (
      d3.select(`#zone-${zoneIndex}`).node() as SVGGElement
    ).getBBox();
    this.zonePositionsMap.set(zoneIndex, domRect);

    return domRect;
  }

  get rootPosition(): [number, number] {
    const rootZone = this.zoneFunction;
    const pos = this.getZonePosition(rootZone.index);
    return [pos.x, pos.y];
  }

  // get current zone html elment from mouse position
  getZoneElementFromXY(x: number, y: number): HTMLElement | null | undefined {
    return document
      .elementsFromPoint(x, y)
      .find((element) => element.classList.contains(ZONE_CLASSES.areaZonePath))
      ?.parentElement;
  }

  // get current zone number from mouse position
  getZoneNumberFromXY(x: number, y: number): number | null {
    const zoneGElement = this.getZoneElementFromXY(x, y);

    if (!zoneGElement) return null;

    return parseInt(zoneGElement.getAttribute(ZONE_CLASSES.areaZoneId)!);
  }
}
