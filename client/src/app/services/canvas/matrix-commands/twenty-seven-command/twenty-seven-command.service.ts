import { Injectable } from '@angular/core';
import { TwentySevenZones } from 'src/app/classes/matrix/zone-strategies/27-zones/27-zones.class';
import { IMatrixCommand } from 'src/app/interfaces/matrix/matrix-command.interface';
import { CanvasBuilderService } from '../../canvas-builder/canvas-builder.service';
import { DrawingMatrixService } from '../../drawing-matrix/drawing-matrix.service';
import { MatrixBuilderService } from '../../matrix-builder/matrix-builder.service';
import { ZoneStrategyService } from '../../zone-strategy/zone-strategy.service';

@Injectable({
  providedIn: 'root',
})
export class TwentySevenCommandService implements IMatrixCommand {
  constructor(
    private matrixBuilderService: MatrixBuilderService,
    private drawingMatrixService: DrawingMatrixService,
    private canvasBuilderService: CanvasBuilderService,
    private zoneStrategyService: ZoneStrategyService
  ) {}

  execute(): void {
    this.canvasBuilderService.removeCanvas();
    this.canvasBuilderService.initCanvas();

    this.zoneStrategyService.setZoneStrategy(
      new TwentySevenZones(
        this.canvasBuilderService.width,
        this.canvasBuilderService.height
      )
    );

    this.openMatrix();
  }

  private openMatrix(): void {
    this.matrixBuilderService.build();
    this.drawingMatrixService.drawRootPostit();
    this.drawingMatrixService.drawAllPostits();
  }
}
