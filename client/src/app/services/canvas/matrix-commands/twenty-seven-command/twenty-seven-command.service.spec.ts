import { TestBed } from '@angular/core/testing';

import { TwentySevenCommandService } from './twenty-seven-command.service';

describe('TwentySevenCommandService', () => {
  let service: TwentySevenCommandService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TwentySevenCommandService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
