import { TestBed } from '@angular/core/testing';

import { MatrixCommandsService } from './matrix-commands.service';

describe('MatrixCommandsService', () => {
  let service: MatrixCommandsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatrixCommandsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
