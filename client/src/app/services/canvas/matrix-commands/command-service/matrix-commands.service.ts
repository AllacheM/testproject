import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { MatrixDrawingSocketService } from 'src/app/services/socket/matrix-drawing-socket/matrix-drawing-socket.service';
import { SocketUtilService } from 'src/app/services/socket/socket-util/socket-util.service';
import {
  MatrixStates,
  MatrixStateService,
} from '../../matrix-state/matrix-state-service/matrix-state.service';
import { ProjectMatrixService } from '../../project-matrix/project-matrix.service';
import { ZoneStrategyService } from '../../zone-strategy/zone-strategy.service';
import { ThreeCommandService } from '../three-command/three-command.service';
import { TwentySevenCommandService } from '../twenty-seven-command/twenty-seven-command.service';

@Injectable({
  providedIn: 'root',
})
export class MatrixCommandsService {
  matrixId!: string;
  matrixReadySubject: Subject<boolean>;

  constructor(
    private projectMatrixService: ProjectMatrixService,
    private matrixDrawingSocketService: MatrixDrawingSocketService,
    private socketUtilService: SocketUtilService,
    private zoneStrategyService: ZoneStrategyService,
    private threeCommandService: ThreeCommandService,
    private twentySevenCommandService: TwentySevenCommandService,
    private matrixStateService: MatrixStateService
  ) {
    this.matrixReadySubject = new Subject();
  }

  destroy(): void {
    this.matrixDrawingSocketService.destroy();
  }

  setMatrixId(id: string) {
    this.matrixId = id;
  }

  notifyIsReady(): void {
    this.connectSocket(this.matrixId);
    this.matrixReadySubject.next(true);
  }

  openMatrix(): void {
    this.projectMatrixService.fetchProjectMatrix(this.matrixId, () => {
      this.matrixStateService.setState(MatrixStates.brainstorming_27);
      this.twentySevenCommandService.execute();
      this.notifyIsReady();
    });
  }

  openOrgan(organIndex: number): boolean {
    const err =
      !this.matrixId ||
      !organIndex ||
      !this.zoneStrategyService.isOrganValid(organIndex);

    if (err) return true;

    this.projectMatrixService.fetchProjectMatrix(this.matrixId, () => {
      this.threeCommandService.setZoneStrategy(organIndex);
      const organ = this.zoneStrategyService.getOrgan(organIndex);

      this.matrixStateService.setState(MatrixStates.brainstorming_3);
      this.projectMatrixService.setProjectMatrixByZones(organ.zones);
      this.threeCommandService.execute();
      this.notifyIsReady();
    });

    return false;
  }

  private connectSocket(matrixId: string): void {
    this.matrixDrawingSocketService.connect();
    this.matrixDrawingSocketService.joinRoom(
      this.socketUtilService.getRoomJWT(matrixId)
    );
  }

  getMatrixReadySubject(): Observable<boolean> {
    return this.matrixReadySubject.asObservable();
  }
}
