import { TestBed } from '@angular/core/testing';

import { ThreeCommandService } from './three-command.service';

describe('ThreeCommandService', () => {
  let service: ThreeCommandService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ThreeCommandService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
