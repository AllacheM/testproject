import { Injectable } from '@angular/core';
import { ThreeZones } from 'src/app/classes/matrix/zone-strategies/3-zones/3-zones.class';
import { IMatrixCommand } from 'src/app/interfaces/matrix/matrix-command.interface';
import { CanvasBuilderService } from '../../canvas-builder/canvas-builder.service';
import { DrawingMatrixService } from '../../drawing-matrix/drawing-matrix.service';
import { MatrixBuilderService } from '../../matrix-builder/matrix-builder.service';
import { ZoneStrategyService } from '../../zone-strategy/zone-strategy.service';

@Injectable({
  providedIn: 'root',
})
export class ThreeCommandService implements IMatrixCommand {
  constructor(
    private matrixBuilderService: MatrixBuilderService,
    private drawingMatrixService: DrawingMatrixService,
    private canvasBuilderService: CanvasBuilderService,
    private zoneStrategyService: ZoneStrategyService
  ) {}

  setZoneStrategy(organIndex: number) {
    this.canvasBuilderService.removeCanvas();
    this.canvasBuilderService.initCanvas();

    this.zoneStrategyService.setZoneStrategy(
      new ThreeZones(
        organIndex,
        this.canvasBuilderService.width,
        this.canvasBuilderService.height
      )
    );
  }

  execute(): void {
    this.openMatrix();
  }

  private openMatrix(): void {
    this.matrixBuilderService.buildWithoutFunction();
    this.drawingMatrixService.drawAllPostits();
  }
}
