import { Injectable } from '@angular/core';
import { POSTITS_CLASSES, POSTITS_SIZE } from 'src/app/classes/utils/zone.util';
import * as d3 from 'd3';
import { CanvasBuilderService } from '../canvas-builder/canvas-builder.service';
import { ProjectMatrixService } from '../project-matrix/project-matrix.service';
import { ZoneStrategyService } from '../zone-strategy/zone-strategy.service';
import { IPostit } from 'src/app/interfaces/matrix/project-matrix.interface';
import { MatrixStateService } from '../matrix-state/matrix-state-service/matrix-state.service';
import { MatrixDrawingSocketService } from '../../socket/matrix-drawing-socket/matrix-drawing-socket.service';
import { D3Utils } from 'src/app/classes/utils/d3.util';
import { d3G } from 'src/app/declaration/types/d3.types';

@Injectable({
  providedIn: 'root',
})
export class DrawingMatrixService {
  private postitForeignMap: Map<number, [d3G, number, number]>;

  constructor(
    private canvasBuilderService: CanvasBuilderService,
    private projectMatrixService: ProjectMatrixService,
    private zoneStrategyService: ZoneStrategyService,
    private matrixStateService: MatrixStateService,
    private matrixDrawingSocketService: MatrixDrawingSocketService
  ) {
    this.postitForeignMap = new Map();
    this.handleSubscriptions();
  }

  private handleSubscriptions(): void {
    this.projectMatrixService.newPostitObservable.subscribe({
      next: (postit: IPostit) => {
        this.drawPostit(postit);
      },
    });
  }

  private getPostitHolder(id: number): string {
    return `postit-holder-${id}`;
  }

  drawRootPostit() {
    this.drawPostit(this.projectMatrixService.rootPostit);
    this.resizePostit(this.projectMatrixService.rootPostit, true);
  }

  drawAllPostits() {
    this.projectMatrixService.postits.forEach((postit) =>
      this.drawPostit(postit)
    );
  }

  drawPostit(postit: IPostit | undefined): void {
    if (!postit) return;
    if (!postit.isRoot && !this.projectMatrixService.contains(postit)) return;

    const postitContainer = this.canvasBuilderService
      .getSvg()
      .selectAll('anything')
      .data([postit])
      .enter()
      .append('g')
      .attr('class', POSTITS_CLASSES.postitGContainer)
      .attr('id', (d) => D3Utils.getPostit(d.id))
      .call(
        d3
          .drag<any, IPostit>()
          .on('start', (e, d) => this.dragStart(e, d))
          .on('drag', (e, d) => this.dragging(e, d))
          .on('end', (e, d) => this.dragEnd(e, d)) as any
      )
      .on('contextmenu', this.matrixStateService.getState().contextMenu)
      .append('foreignObject')
      .attr('id', (d) => D3Utils.getPostitForeignObject(d.id))
      .attr('width', POSTITS_SIZE.maxWidth)
      .attr('height', POSTITS_SIZE.maxHeight)
      .attr('x', (d) => this.projectMatrixService.calcRelativeXToPixels(d))
      .attr('y', (d) => this.projectMatrixService.calcRelativeYToPixels(d))
      .append('xhtml:div')
      .attr(
        'class',
        (d) => `${POSTITS_CLASSES.postitMainContainer} ${d.cssClass}`
      )
      .attr('id', (d) => this.getPostitHolder(d.id))
      .append('xhtml:div')
      .attr('class', POSTITS_CLASSES.postitHolder);

    // zone nb
    postitContainer
      .append('small')
      .attr('class', POSTITS_CLASSES.postitZoneNb)
      .text((d) => d.zone);

    // postit text
    postitContainer
      .append('p')
      .attr('class', POSTITS_CLASSES.postitText)
      .text((d) => d.text);

    this.resizePostit(postit);
  }

  dragStart(event: DragEvent, d: IPostit) {
    this.matrixDrawingSocketService.sendDragStartPostit(d);
  }

  dragging(event: DragEvent, d: IPostit) {
    const [x, y] = d3.pointer(event);
    const newZone = this.zoneStrategyService.getZoneNumberFromXY(x, y);

    if (!newZone) return;

    let [pointerX, pointerY] = d3.pointer(
      event,
      this.zoneStrategyService.getZoneElementFromXY(x, y)
    );

    const [_, width, height] = this.postitForeignMap.get(d.id)!;
    pointerX -= width / 2;
    pointerY -= height / 2;

    const zoneRect = this.zoneStrategyService.getZonePosition(newZone);

    const nextCornerX = zoneRect.x + zoneRect.width;
    const relX = 1 - (nextCornerX - pointerX) / zoneRect.width;

    const nextCornerY = zoneRect.y + zoneRect.height;
    const relY = 1 - (nextCornerY - pointerY) / zoneRect.height;

    d.zone = newZone;
    d.x = relX;
    d.y = relY;

    this.matrixDrawingSocketService.sendDraggingPostit(d);
  }

  dragEnd(event: DragEvent, d: IPostit) {
    this.matrixDrawingSocketService.sendDragEndPostit(d);
  }

  createNewPostit(postit: Partial<IPostit>): void {
    postit.x = 0.05;
    postit.y = 0.05;

    this.matrixDrawingSocketService.sendNewPostitCreated(postit as IPostit);
  }

  // resize postit after draw
  private resizePostit(postit: IPostit | undefined, isRoot = false): void {
    if (!postit) return;

    const postitHolder = d3.select(`#${this.getPostitHolder(postit.id)}`);
    const foreignObject = d3.select(
      `#${D3Utils.getPostitForeignObject(postit.id)}`
    );

    const width = parseInt(postitHolder.style('width'));
    const height = parseInt(postitHolder.style('height'));

    foreignObject.attr('width', width);
    foreignObject.attr('height', height);

    this.postitForeignMap.set(postit.id, [foreignObject, width, height]);

    if (isRoot) {
      foreignObject
        .attr('x', () =>
          isRoot
            ? this.zoneStrategyService.rootPosition[0] +
              this.zoneStrategyService.zoneWidthSize / 2 -
              width / 2
            : postit.x
        )
        .attr('y', () =>
          isRoot ? this.zoneStrategyService.rootPosition[1] + 10 : postit.y
        );
    }
  }
}
