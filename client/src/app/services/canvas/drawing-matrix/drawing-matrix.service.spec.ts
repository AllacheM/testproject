import { TestBed } from '@angular/core/testing';

import { DrawingMatrixService } from './drawing-matrix.service';

describe('DrawingMatrixService', () => {
  let service: DrawingMatrixService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DrawingMatrixService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
