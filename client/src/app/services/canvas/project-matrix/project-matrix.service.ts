import { Injectable } from '@angular/core';
import {
  IPostit,
  IProjectMatrix,
} from 'src/app/interfaces/matrix/project-matrix.interface';
import { Observable, Subject } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { IHttpResponse } from 'src/app/interfaces/http.interface';
import { HttpService } from '../../http/http.service';
import { AuthStorageService } from '../../local-storage/auth-storage/auth-storage.service';
import { RouterService } from '../../router/router.service';
import { SnackbarService } from '../../snackbar/snackbar.service';
import { ZoneStrategyService } from '../zone-strategy/zone-strategy.service';
import { D3Utils } from 'src/app/classes/utils/d3.util';
import * as d3 from 'd3';

@Injectable({
  providedIn: 'root',
})
export class ProjectMatrixService {
  private projectMatrix!: IProjectMatrix;
  private postitSubject: Subject<IPostit>;

  constructor(
    private httpService: HttpService,
    private authStorageService: AuthStorageService,
    private routerService: RouterService,
    private snackbarService: SnackbarService,
    private zoneStrategyService: ZoneStrategyService
  ) {
    this.postitSubject = new Subject();
  }

  setProjectMatrix(matrix: IProjectMatrix): void {
    this.projectMatrix = matrix;
  }

  getProjectMatrix(): IProjectMatrix {
    return this.projectMatrix;
  }

  contains(postit: IPostit): boolean {
    return this.postits.some((p) => p.id === postit.id);
  }

  fetchProjectMatrix(matrixId: string, openMatrixCb: () => void): void {
    const endpoint = `get_matrix/${this.authStorageService.get()!}/${matrixId}`;

    this.httpService.get<IHttpResponse<IProjectMatrix>>(endpoint).subscribe({
      next: (data: HttpResponse<IHttpResponse<IProjectMatrix>>) => {
        if (data.body?.code === 3) {
          this.onFetchMatrixError();
          return;
        }

        const project = data.body?.data as IProjectMatrix;
        this.zoneStrategyService.resetMap();
        this.setProjectMatrix(project);
        openMatrixCb();
      },
      error: () => this.onFetchMatrixError(),
    });
  }

  private onFetchMatrixError() {
    this.snackbarService.showErrorWithoutAction('Cartographie introuvable!');
    this.routerService.navigateToHome();
  }

  addNewPostit(postit: IPostit) {
    this.projectMatrix.postits.push(postit as IPostit);
    this.postitSubject.next(postit as IPostit);
  }

  removePostit(postit: IPostit) {
    this.projectMatrix.postits = this.projectMatrix.postits.filter(
      (p) => p.id !== postit.id
    );

    d3.select(`#${D3Utils.getPostitForeignObject(postit.id)}`).remove();
  }

  setProjectMatrixByZones(zones: number[]) {
    this.projectMatrix.postits = this.projectMatrix.postits.filter((postit) =>
      zones.includes(postit.zone)
    );
  }

  calcRelativeXToPixels(postit: IPostit): number {
    const zonePosition = this.zoneStrategyService.getZonePosition(postit.zone);
    const x = zonePosition.x + zonePosition.width * postit.x;
    return x;
  }

  calcRelativeYToPixels(postit: IPostit): number {
    const zonePosition = this.zoneStrategyService.getZonePosition(postit.zone);
    const y = zonePosition.y + zonePosition.height * postit.y;
    return y;
  }

  get rootPostit(): IPostit | undefined {
    return this.getProjectMatrix().root;
  }

  get postits(): IPostit[] {
    return this.getProjectMatrix().postits;
  }

  get cartographyQuestion() {
    return this.projectMatrix.cartography.question;
  }

  get newPostitObservable(): Observable<IPostit> {
    return this.postitSubject.asObservable();
  }
}
