import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IHttpResponse } from 'src/app/interfaces/http.interface';
import { HttpService } from '../http/http.service';
import { AuthStorageService } from '../local-storage/auth-storage/auth-storage.service';

@Injectable({
  providedIn: 'root',
})
export class ProjectCreationService {
  constructor(
    private httpService: HttpService,
    private authStorageService: AuthStorageService
  ) {}

  createRootProject(
    fonction: string,
    question: string
  ): Observable<HttpResponse<IHttpResponse<any>>> {
    const formData = new FormData();
    formData.append('jwt', this.authStorageService.get()!);
    formData.append('fonction', fonction);
    formData.append('question', question);

    return this.httpService.post<FormData, IHttpResponse<any>>(
      'create_root_project',
      formData
    );
  }
}
