import { TestBed } from '@angular/core/testing';

import { ProjectCreationService } from './project-creation.service';

describe('ProjectCreationService', () => {
  let service: ProjectCreationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProjectCreationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
