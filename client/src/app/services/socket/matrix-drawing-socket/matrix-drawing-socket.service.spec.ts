import { TestBed } from '@angular/core/testing';

import { MatrixDrawingSocketService } from './matrix-drawing-socket.service';

describe('MatrixDrawingSocketService', () => {
  let service: MatrixDrawingSocketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatrixDrawingSocketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
