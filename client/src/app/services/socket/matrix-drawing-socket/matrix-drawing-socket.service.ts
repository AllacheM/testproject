import { Injectable } from '@angular/core';
import { AbsSocketService } from 'src/app/classes/socket/abs-socket.class';
import {
  DRAGGING_POSTIT_EVENT,
  DRAG_END_POSTIT_EVENT,
  DRAG_START_POSTIT_EVENT,
  NEW_POSTIT_EVENT,
} from 'src/app/declaration/socket/socket-room.constants';
import { IPostit } from 'src/app/interfaces/matrix/project-matrix.interface';
import {
  ISocketDataListener,
  ISocketRoom,
} from 'src/app/interfaces/socket-room.interface';
import { MatrixSynchronizationService } from '../matrix-synchronization/matrix-synchronization.service';
import { SocketUtilService } from '../socket-util/socket-util.service';

@Injectable({
  providedIn: 'root',
})
export class MatrixDrawingSocketService extends AbsSocketService {
  private socketRoom!: ISocketRoom;

  constructor(
    private matrixSynchronizationService: MatrixSynchronizationService,
    private socketUtilService: SocketUtilService
  ) {
    super();
    this.init();
  }

  override joinRoom(socketRoom: ISocketRoom) {
    this.socketRoom = socketRoom;
    super.joinRoom(socketRoom);
  }

  destroy(): void {
    if (this.socketRoom && this.socketRoom.roomName) {
      super.leaveRoom(this.socketRoom);
      this.socketRoom = {} as ISocketRoom;
    }

    this.disconnect();
  }

  get roomName(): string {
    return this.socketRoom.roomName;
  }

  // incoming sockets emitters
  protected setSocketListens(): void {
    this.listenNewPostitCreated();
    this.listenPostitDragStart();
    this.listenPostitDragging();
    this.listenPostitDragEnd();
  }

  private listenNewPostitCreated(): void {
    this.namespaceSocket.on(
      NEW_POSTIT_EVENT,
      (data: ISocketDataListener<IPostit>) => {
        this.matrixSynchronizationService.createPostit(data.data);
      }
    );
  }

  private listenPostitDragStart(): void {
    this.namespaceSocket.on(
      DRAG_START_POSTIT_EVENT,
      (data: ISocketDataListener<IPostit>) => {
        this.matrixSynchronizationService.onDragStart(data.data);
      }
    );
  }

  private listenPostitDragging(): void {
    this.namespaceSocket.on(
      DRAGGING_POSTIT_EVENT,
      (data: ISocketDataListener<IPostit>) => {
        this.matrixSynchronizationService.onDragging(data.data);
      }
    );
  }

  private listenPostitDragEnd(): void {
    this.namespaceSocket.on(
      DRAG_END_POSTIT_EVENT,
      (data: ISocketDataListener<IPostit>) => {
        this.matrixSynchronizationService.onDragEnd(data.data);
      }
    );
  }

  sendNewPostitCreated(postit: IPostit): void {
    this.emit(
      NEW_POSTIT_EVENT,
      this.socketUtilService.getDataJWT(postit, this.roomName)
    );
  }

  sendDragStartPostit(postit: IPostit): void {
    this.emit(
      DRAG_START_POSTIT_EVENT,
      this.socketUtilService.getDataJWT(postit, this.roomName)
    );
  }

  sendDraggingPostit(postit: IPostit): void {
    this.emit(
      DRAGGING_POSTIT_EVENT,
      this.socketUtilService.getDataJWT(postit, this.roomName)
    );
  }

  sendDragEndPostit(postit: IPostit): void {
    this.emit(
      DRAG_END_POSTIT_EVENT,
      this.socketUtilService.getDataJWT(postit, this.roomName)
    );
  }
}
