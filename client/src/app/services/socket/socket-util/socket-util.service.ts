import { Injectable } from '@angular/core';
import {
  ISocketDataEmitter,
  ISocketRoom,
} from 'src/app/interfaces/socket-room.interface';
import { AuthStorageService } from '../../local-storage/auth-storage/auth-storage.service';

@Injectable({
  providedIn: 'root',
})
export class SocketUtilService {
  constructor(private authStorageService: AuthStorageService) {}

  getDataJWT(data: any, roomName: string): ISocketDataEmitter {
    return {
      jwt: this.authStorageService.get()!,
      data,
      roomName,
    };
  }

  getRoomJWT(roomId: string): ISocketRoom {
    return {
      jwt: this.authStorageService.get()!,
      roomName: roomId,
    };
  }
}
