import { TestBed } from '@angular/core/testing';

import { SocketUtilService } from './socket-util.service';

describe('SocketUtilService', () => {
  let service: SocketUtilService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SocketUtilService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
