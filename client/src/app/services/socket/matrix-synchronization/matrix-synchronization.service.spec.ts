import { TestBed } from '@angular/core/testing';

import { MatrixSynchronizationService } from './matrix-synchronization.service';

describe('MatrixSynchronizationService', () => {
  let service: MatrixSynchronizationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatrixSynchronizationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
