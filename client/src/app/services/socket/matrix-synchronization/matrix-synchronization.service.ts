import { Injectable } from '@angular/core';
import { IPostit } from 'src/app/interfaces/matrix/project-matrix.interface';
import * as d3 from 'd3';
import { POSTITS_CLASSES } from 'src/app/classes/utils/zone.util';
import { D3Utils } from 'src/app/classes/utils/d3.util';
import { ProjectMatrixService } from '../../canvas/project-matrix/project-matrix.service';
import { ZoneStrategyService } from '../../canvas/zone-strategy/zone-strategy.service';

@Injectable({
  providedIn: 'root',
})
export class MatrixSynchronizationService {
  private draggingCssClass = 'dragging';

  constructor(
    private projectMatrixService: ProjectMatrixService,
    private zoneStrategyService: ZoneStrategyService
  ) {}

  createPostit(postit: IPostit) {
    this.projectMatrixService.addNewPostit(postit);
  }

  onDragStart(postit: IPostit) {
    d3.select(`#${D3Utils.getPostit(postit.id)}`).classed(
      this.draggingCssClass,
      true
    );
  }

  onDragging(postit: IPostit) {
    const doesPostitExist = this.projectMatrixService.contains(postit);

    if (!this.zoneStrategyService.isZoneInOrgan(postit.zone)) {
      if (doesPostitExist) {
        this.projectMatrixService.removePostit(postit);
      }

      return;
    }

    if (!doesPostitExist) {
      this.projectMatrixService.addNewPostit(postit);
    }

    d3.select(`#${D3Utils.getPostitForeignObject(postit.id)}`)
      .attr('x', () => this.projectMatrixService.calcRelativeXToPixels(postit))
      .attr('y', () => this.projectMatrixService.calcRelativeYToPixels(postit))
      .select(
        `#${D3Utils.getPostit(postit.id)} .${POSTITS_CLASSES.postitZoneNb}`
      )
      .text(postit.zone);
  }

  onDragEnd(postit: IPostit) {
    d3.select(`#${D3Utils.getPostit(postit.id)}`).classed(
      this.draggingCssClass,
      false
    );
  }
}
