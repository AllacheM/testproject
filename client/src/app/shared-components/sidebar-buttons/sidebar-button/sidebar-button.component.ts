import { Component, OnInit, Input } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { SidebarService } from 'src/app/services/sidebar/sidebar.service';

@Component({
  selector: 'app-sidebar-button',
  templateUrl: './sidebar-button.component.html',
  styleUrls: ['./sidebar-button.component.scss'],
})
export class SidebarButtonComponent implements OnInit {
  @Input() faIcon!: IconDefinition;
  @Input() title!: string;
  @Input() toolTip!: string;
  @Input() callback!: () => void;
  openSidebar = false;

  constructor(private sidebarService: SidebarService) {
    this.subscribeToSidebar();
  }

  ngOnInit(): void {}

  onClicked() {
    this.callback();
  }

  subscribeToSidebar(): void {
    this.sidebarService
      .getObservable()
      .subscribe((isOpen: boolean) => (this.openSidebar = isOpen));
  }
}
