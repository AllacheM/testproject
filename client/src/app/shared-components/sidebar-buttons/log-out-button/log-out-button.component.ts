import { Component, OnInit } from '@angular/core';
import { faRightFromBracket } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-log-out-button',
  templateUrl: './log-out-button.component.html',
  styleUrls: ['./log-out-button.component.scss'],
})
export class LogOutButtonComponent implements OnInit {
  faRightFromBracket = faRightFromBracket;
  onLogOutCb!: () => void;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.onLogOutCb = this.onLogOut.bind(this);
  }

  onLogOut(): void {
    this.authService.logOut();
  }
}
