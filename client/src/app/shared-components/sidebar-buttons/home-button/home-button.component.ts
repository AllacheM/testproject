import { Component, OnInit } from '@angular/core';
import { RouterService } from 'src/app/services/router/router.service';
import { faHome } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-home-button',
  templateUrl: './home-button.component.html',
  styleUrls: ['./home-button.component.scss'],
})
export class HomeButtonComponent implements OnInit {
  faHome = faHome;
  goToHomeCb!: () => void;

  constructor(private routerService: RouterService) {}

  ngOnInit(): void {
    this.goToHomeCb = this.goToHome.bind(this);
  }

  goToHome(): void {
    this.routerService.navigateToHome();
  }
}
