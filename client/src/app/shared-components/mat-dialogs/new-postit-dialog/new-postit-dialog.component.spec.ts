import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPostitDialogComponent } from './new-postit-dialog.component';

describe('NewPostitDialogComponent', () => {
  let component: NewPostitDialogComponent;
  let fixture: ComponentFixture<NewPostitDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewPostitDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPostitDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
