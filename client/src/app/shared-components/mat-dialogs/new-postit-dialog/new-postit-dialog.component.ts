import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import {
  IPostit,
  POSTIT_CSS_CLASSES,
} from 'src/app/interfaces/matrix/project-matrix.interface';
import { IZone } from 'src/app/interfaces/zone/zone-path.interface';
import { DrawingMatrixService } from 'src/app/services/canvas/drawing-matrix/drawing-matrix.service';
import { ZoneStrategyService } from 'src/app/services/canvas/zone-strategy/zone-strategy.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';

@Component({
  selector: 'app-new-postit-dialog',
  templateUrl: './new-postit-dialog.component.html',
  styleUrls: ['./new-postit-dialog.component.scss'],
})
export class NewPostitDialogComponent implements OnInit {
  postitForm: FormGroup;
  postitClasses = Object.values(POSTIT_CSS_CLASSES);
  check = faCheck;
  zones: IZone[];

  constructor(
    private dialogRef: MatDialogRef<NewPostitDialogComponent>,
    private drawingMatrixService: DrawingMatrixService,
    private snackbarService: SnackbarService,
    private zoneStrategyService: ZoneStrategyService
  ) {
    this.zones = this.zoneStrategyService.areaDataWithoutFunction;
    this.postitForm = new FormGroup({
      cssClass: new FormControl(POSTIT_CSS_CLASSES.orange, [
        Validators.required,
      ]),
      text: new FormControl('', [Validators.required]),
      zoneId: new FormControl(1, [Validators.required]),
    });
  }

  ngOnInit(): void {}

  onCreate(event: MouseEvent) {
    event.preventDefault();

    const postit: Partial<IPostit> = {
      cssClass: this.postitForm.get('cssClass')?.value,
      text: this.postitForm.get('text')?.value,
      zone: parseInt(this.postitForm.get('zoneId')?.value),
    };

    this.drawingMatrixService.createNewPostit(postit);
    this.close();
    this.snackbarService.showSuccessWithoutAction(
      `Post-it ajouté dans la zone #${postit.zone}`
    );
  }

  close() {
    this.dialogRef.close();
  }

  setCssClass(postitClass: string) {
    this.postitForm.get('cssClass')?.setValue(postitClass);
  }

  get cssClass() {
    return this.postitForm.get('cssClass')!.value;
  }
}
