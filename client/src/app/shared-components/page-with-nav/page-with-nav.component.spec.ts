import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageWithNavComponent } from './page-with-nav.component';

describe('PageWithNavComponent', () => {
  let component: PageWithNavComponent;
  let fixture: ComponentFixture<PageWithNavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageWithNavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageWithNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
