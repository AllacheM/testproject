import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { IHttpResponse } from 'src/app/interfaces/http.interface';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-change-password-bs',
  templateUrl: './change-password-bs.component.html',
  styleUrls: ['./change-password-bs.component.scss']
})
export class ChangePasswordBSComponent implements OnInit {

  hideOld = true;
  hideNew = true;
  hideConfirmation = true;
  eyeHideIcon = faEyeSlash;
  eyeShowIcon = faEye;

  public changePwdForm!: FormGroup;

  constructor(private userService: UserService,
    private snackbarService: SnackbarService,
    private bottomSheetRef: MatBottomSheetRef<ChangePasswordBSComponent>) {
  }

  ngOnInit(): void {
    this.changePwdForm = new FormGroup({
      oldPwd: new FormControl("", [
        Validators.required]),
      newPwd: new FormControl("", [
        Validators.required,
        Validators.minLength(8)]),
      confirmPwd: new FormControl("", [
        Validators.required])
    },
      [CustomValidators.matchingPassword('newPwd', 'confirmPwd')]
    )
  }

  get oldPwd() {
    return this.changePwdForm.get('oldPwd')!;
  }

  get newPwd() {
    return this.changePwdForm.get('newPwd')!;
  }

  get confirmPwd() {
    return this.changePwdForm.get('confirmPwd')!;
  }

  onHidePassword(event: MouseEvent, hide: boolean): boolean {
    event.preventDefault();
    return !hide;
  }

  confirm(event: Event): void {
    event.preventDefault();
    this.userService.changePassword(this.changePwdForm.get('oldPwd')?.value,
      this.changePwdForm.get('newPwd')?.value).subscribe({
        next: (response: HttpResponse<IHttpResponse<any>>) => {
          const data = response.body;
  
          if (data?.code === 2 || data?.code === 3) {
            this.snackbarService.showErrorWithoutAction(data?.message!);
            return;
          }
          this.bottomSheetRef.dismiss();
          this.snackbarService.showSuccessWithoutAction(
            'Mot de passe changé!'
          );
        },
        error: (error: HttpErrorResponse) => {
          this.snackbarService.showErrorWithoutAction(
            `Erreur! Veuillez réessayer!`
          );
        },
      });
  }
}

export class CustomValidators {
  static matchingPassword(newPass: string, confirmPass: string): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return control.get(newPass) && control.get(confirmPass)
        && control.get(newPass)?.value !== control.get(confirmPass)?.value
        ? { passwordNotMatch: true }
        : null;
    };
  }
}
