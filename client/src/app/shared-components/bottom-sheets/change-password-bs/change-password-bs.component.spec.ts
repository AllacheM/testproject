import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePasswordBSComponent } from './change-password-bs.component';

describe('ChangePasswordBSComponent', () => {
  let component: ChangePasswordBSComponent;
  let fixture: ComponentFixture<ChangePasswordBSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangePasswordBSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasswordBSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
