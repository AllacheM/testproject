import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAccountBSComponent } from './create-account-bs.component';

describe('CreateAccountBSComponent', () => {
  let component: CreateAccountBSComponent;
  let fixture: ComponentFixture<CreateAccountBSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateAccountBSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAccountBSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
