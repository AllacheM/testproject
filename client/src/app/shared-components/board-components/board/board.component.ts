import { OnInit, Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { faNoteSticky } from '@fortawesome/free-regular-svg-icons';
import {
  faArrowPointer,
  faBackward,
  IconDefinition,
} from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { IZone } from 'src/app/interfaces/zone/zone-path.interface';
import { CanvasBuilderService } from 'src/app/services/canvas/canvas-builder/canvas-builder.service';
import { MatrixCommandsService } from 'src/app/services/canvas/matrix-commands/command-service/matrix-commands.service';
import { MatrixStateService } from 'src/app/services/canvas/matrix-state/matrix-state-service/matrix-state.service';
import { MatDialogService } from 'src/app/services/mat-dialog/mat-dialog.service';
import { RouterService } from 'src/app/services/router/router.service';
import { NewPostitDialogComponent } from '../../mat-dialogs/new-postit-dialog/new-postit-dialog.component';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {
  scaleValue: number = 0;
  zone: Partial<IZone> = {};
  isMakingRequest: boolean = true;
  matrixId!: string;

  arrowPointer = faArrowPointer;
  postitIcon = faNoteSticky;
  backwardIcon: IconDefinition | undefined;

  private subscriptions: Subscription[] = [];

  constructor(
    private canvasBuilderService: CanvasBuilderService,
    private activatedRoute: ActivatedRoute,
    private matDialogService: MatDialogService,
    private matrixStateService: MatrixStateService,
    private matrixCommandsService: MatrixCommandsService,
    private routerService: RouterService
  ) {
    this.subscriptions.push(
      this.matrixCommandsService.getMatrixReadySubject().subscribe({
        next: () => this.onIsReady(),
      })
    );
  }

  ngOnInit(): void {
    window.onbeforeunload = () => this.ngOnDestroy();
    this.handleDefaultSubscriptions();
  }

  ngOnDestroy() {
    this.matrixCommandsService.destroy();
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  private leaveRoom() {
    this.isMakingRequest = true;

    if (this.matrixId) {
      this.matrixCommandsService.destroy();
    }
  }

  private handleOnIdChange(matrixId: string) {
    this.leaveRoom();

    this.matrixId = matrixId;
    this.backwardIcon = undefined;
    this.matrixCommandsService.setMatrixId(this.matrixId);

    if (this.activatedRoute.snapshot.queryParamMap.has('organIndex')) return;
    this.matrixCommandsService.openMatrix();
  }

  private handleOnOrganIdChange(organIndex: string) {
    this.leaveRoom();

    if (!organIndex) {
      this.handleOnIdChange(this.matrixId);
      return;
    }

    const invalidOrgan = this.matrixCommandsService.openOrgan(
      parseInt(organIndex)
    );

    if (invalidOrgan) {
      this.openWholeMatrix();
      return;
    }

    this.backwardIcon = faBackward;
  }

  private onIsReady() {
    this.isMakingRequest = false;
    this.handleCanvasSubscriptions();
  }

  private handleDefaultSubscriptions() {
    this.subscriptions.push(
      this.activatedRoute.params.subscribe((params: Params) =>
        this.handleOnIdChange(params['id'])
      )
    );

    this.subscriptions.push(
      this.activatedRoute.queryParams.subscribe((params: Params) =>
        this.handleOnOrganIdChange(params['organIndex'])
      )
    );
  }

  private handleCanvasSubscriptions() {
    this.subscriptions.push(
      this.canvasBuilderService.getCanvasZoomSubject().subscribe({
        next: (scaleValue: number) => (this.scaleValue = scaleValue),
      })
    );

    this.subscriptions.push(
      this.matrixStateService.getState().mouseOverAreaObservable.subscribe({
        next: (zone: Partial<IZone>) =>
          (this.zone = zone.isFunction ? {} : zone),
      })
    );
  }

  openWholeMatrix(): void {
    this.isMakingRequest = true;
    this.routerService.addQueryParamsToUrl({});
  }

  openNewPostitModal(): void {
    this.matDialogService.open(NewPostitDialogComponent);
  }
}
