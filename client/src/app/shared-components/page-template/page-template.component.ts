import { Component, OnInit } from '@angular/core';
import {
  faBars,
  faCircleXmark,
  faUser,
} from '@fortawesome/free-solid-svg-icons';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { RouterService } from 'src/app/services/router/router.service';
import { SidebarService } from 'src/app/services/sidebar/sidebar.service';
import { ChangePasswordBSComponent } from 'src/app/shared-components/bottom-sheets/change-password-bs/change-password-bs.component';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-page-template',
  templateUrl: './page-template.component.html',
  styleUrls: ['./page-template.component.scss'],
})
export class PageTemplateComponent implements OnInit {
  faBars = faBars;
  faCircleXmark = faCircleXmark;
  faUser = faUser;
  openSidebar = false;

  goToProfileCb!: () => void;

  constructor(
    private sidebarService: SidebarService,
    private routerService: RouterService,
    private passwordBottomSheet: MatBottomSheet,
    private userService: UserService
  ) {
    this.subscribeToSidebar();
  }

  ngOnInit(): void {
    this.goToProfileCb = this.goToProfile.bind(this);
  }

  toggleSidebar(): void {
    this.sidebarService.toggle();
  }

  goToProfile(): void {
  }

  isAdmin(): boolean {
    return this.userService.isAdmin;
  }

  subscribeToSidebar(): void {
    this.sidebarService
      .getObservable()
      .subscribe((isOpen: boolean) => (this.openSidebar = isOpen));
  }

  openPasswordBottomSheet(): void {
    this.passwordBottomSheet.open(ChangePasswordBSComponent);
  }
}
