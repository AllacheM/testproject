import { NgModule } from '@angular/core';
import { AngularMaterialModule } from './angular-material.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PageTemplateComponent } from '../shared-components/page-template/page-template.component';
import { SidebarButtonComponent } from '../shared-components/sidebar-buttons/sidebar-button/sidebar-button.component';
import { HomeButtonComponent } from '../shared-components/sidebar-buttons/home-button/home-button.component';
import { ChangePasswordBSComponent } from '../shared-components/bottom-sheets/change-password-bs/change-password-bs.component';
import { CreateAccountBSComponent } from '../shared-components/bottom-sheets/create-account-bs/create-account-bs.component';
import { LogOutButtonComponent } from '../shared-components/sidebar-buttons/log-out-button/log-out-button.component';
import { BoardComponent } from '../shared-components/board-components/board/board.component';
import { NewPostitDialogComponent } from '../shared-components/mat-dialogs/new-postit-dialog/new-postit-dialog.component';
import { PageWithNavComponent } from '../shared-components/page-with-nav/page-with-nav.component';
import { SpinnerComponent } from '../shared-components/spinner/spinner.component';

// add shared-components to be reused in different sub modules

@NgModule({
  declarations: [
    PageTemplateComponent,
    SidebarButtonComponent,
    HomeButtonComponent,
    ChangePasswordBSComponent,
    CreateAccountBSComponent,
    LogOutButtonComponent,
    BoardComponent,
    NewPostitDialogComponent,
    PageWithNavComponent,
    SpinnerComponent,
  ],
  imports: [FontAwesomeModule, AngularMaterialModule],
  providers: [],
  bootstrap: [],
  exports: [
    FontAwesomeModule,
    AngularMaterialModule,
    PageTemplateComponent,
    SidebarButtonComponent,
    HomeButtonComponent,
    PageWithNavComponent,
    LogOutButtonComponent,
    SpinnerComponent,
  ],
})
export class SharedModule {}
