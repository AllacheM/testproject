import { NgModule } from '@angular/core';
import { AdminAuthGuard } from 'src/app/classes/auth-guards/admin-auth.guard';
import { SharedModule } from '../shared-module';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminRoutingModule } from './admin.routing.module';
import { AdminMainAppComponent } from './admin-main-app/admin-main-app.component';
import { CompanyCreationComponent } from '../admin/admin-home/company-creation/company-creation.component';
import { CompanyComponent } from './admin-home/company/company.component';

@NgModule({
  declarations: [
    AdminMainAppComponent,
    AdminHomeComponent,
    CompanyCreationComponent,
    CompanyComponent,
  ],
  imports: [SharedModule, AdminRoutingModule],
  providers: [AdminAuthGuard],
  bootstrap: [],
})
export class AdminModule {}
