import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminMainAppComponent } from './admin-main-app/admin-main-app.component';

const routes: Routes = [
  {
    path: '',
    component: AdminMainAppComponent,
    children: [{ path: '', component: AdminHomeComponent }],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
