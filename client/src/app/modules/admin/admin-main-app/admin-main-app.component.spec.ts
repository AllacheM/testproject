import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMainAppComponent } from './admin-main-app.component';

describe('AdminMainAppComponent', () => {
  let component: AdminMainAppComponent;
  let fixture: ComponentFixture<AdminMainAppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminMainAppComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMainAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
