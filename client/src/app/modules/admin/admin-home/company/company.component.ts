import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ICompany } from 'src/app/interfaces/company.interface';
import { IHttpResponse } from 'src/app/interfaces/http.interface';
import { CompanyService } from 'src/app/services/company/company.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  constructor(private companyService: CompanyService) { }

  ngOnInit(): void { 
    this.companyService.clearCompanies();
    this.companyService.getCompanies().subscribe({
        next: (response: HttpResponse<IHttpResponse<any>>) => {
          const data = response.body;
  
          if (data?.code !== 1) return;
          this.companyService.addCompanies(data);
        }
      });
  }
  
  get companies(): ICompany[]{
    return this.companyService.companies;
  }
}
