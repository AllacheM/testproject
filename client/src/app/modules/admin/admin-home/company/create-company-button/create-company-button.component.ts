import { Component, OnInit } from '@angular/core';
import { MatDialogService } from 'src/app/services/mat-dialog/mat-dialog.service';
import { CompanyCreationComponent } from '../company.component';

@Component({
  selector: 'app-create-company-button',
  templateUrl: './create-company-button.component.html',
  styleUrls: ['./create-company-button.component.scss']
})
export class CreateCompanyButtonComponent implements OnInit {

  constructor( private dialogService: MatDialogService ) { }


  openCreateDialog(): void {
    this.dialogService.open(CompanyCreationComponent);
  }

  ngOnInit(): void {
  }

}
