import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { IHttpResponse } from 'src/app/interfaces/http.interface';
import { CompanyService } from 'src/app/services/company/company.service';

@Component({
  selector: 'app-company-creation',
  templateUrl: './company-creation.component.html',
  styleUrls: ['./company-creation.component.scss']
})
export class CompanyCreationComponent implements OnInit {
  companyCreationForm: FormGroup;
  constructor(private companyService: CompanyService) { 
    this.companyCreationForm = new FormGroup({
      name: new FormControl(''),
      info: new FormControl(''),
    });
  }

  ngOnInit(): void {
  }
  createCompanyClicked(event: MouseEvent){
    event.preventDefault();
    this.companyService.createCompany(
      this.companyCreationForm.get('name')?.value,
      this.companyCreationForm.get('info')?.value).subscribe({
        next: (response: HttpResponse<IHttpResponse<any>>) => {
          const data = response.body;
  
          if (data?.code !== 1) {
            return;
          }
          this.companyService.onSuccessfulCompanyCreation(data);
        }
      });
  }
}
