import { Component, OnInit } from '@angular/core';
import { MatDialogService } from 'src/app/services/mat-dialog/mat-dialog.service';
import { CompanyCreationComponent } from './company-creation/company-creation.component';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss'],
})
export class AdminHomeComponent implements OnInit {
  constructor(private dialogService: MatDialogService) {}

  ngOnInit(): void {}

  openCreateDialog(): void {
    this.dialogService.open(CompanyCreationComponent);
  }
}
