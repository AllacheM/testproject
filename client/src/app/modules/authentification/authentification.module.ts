import { NgModule } from '@angular/core';
import { AuthentificationGuard } from 'src/app/classes/auth-guards/authentification.guard';
import { SharedModule } from '../shared-module';
import { AuthentificationRoutingModule } from './authentification.routing.module';
import { LoginComponent } from './login/login.component';
import { InformationDialogComponent } from '../../shared-components/mat-dialogs/information-dialog/information-dialog.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AuthComponent } from './auth/auth.component';

@NgModule({
  declarations: [LoginComponent, InformationDialogComponent, SignUpComponent, AuthComponent],
  imports: [SharedModule, AuthentificationRoutingModule],
  providers: [AuthentificationGuard],
  bootstrap: [],
})
export class AuthentificationModule {}
