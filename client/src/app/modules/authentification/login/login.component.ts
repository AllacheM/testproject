import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormValidation } from 'src/app/classes/utils/form-validation.class';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { IHttpResponse } from 'src/app/interfaces/http.interface';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MatDialogService } from 'src/app/services/mat-dialog/mat-dialog.service';
import { RouterService } from 'src/app/services/router/router.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import { InformationDialogComponent } from 'src/app/shared-components/mat-dialogs/information-dialog/information-dialog.component';
import { AuthStorageService } from 'src/app/services/local-storage/auth-storage/auth-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  hidePwd = true;
  eyeHideIcon = faEyeSlash;
  eyeShowIcon = faEye;

  constructor(
    private dialogService: MatDialogService,
    private authService: AuthService,
    private authStorageService: AuthStorageService,
    private snackbarService: SnackbarService,
    private routerService: RouterService
  ) {
    this.loginForm = new FormGroup({
      email: new FormControl(
        '',
        Validators.compose([Validators.required, FormValidation.validateEmail])
      ),
      password: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {
    this.authStorageService.remove();
  }

  openInfoDialog(): void {
    this.dialogService.open(InformationDialogComponent);
  }

  onHidePassword(event: MouseEvent): void {
    event.preventDefault();
    this.hidePwd = !this.hidePwd;
  }

  onLoginClicked(event: MouseEvent): void {
    event.preventDefault();

    if (!this.email?.value && !this.password?.value) {
      this.snackbarService.showErrorWithoutAction(
        `Veuillez entrer un nom d'utilisateur et un mot de passe valide`
      );
      return;
    }

    this.authService.login(this.email?.value, this.password?.value).subscribe({
      next: (response: HttpResponse<IHttpResponse<any>>) => {
        const data = response.body;

        if (data?.code !== 1) {
          this.snackbarService.showErrorWithoutAction(data?.message!);
          return;
        }

        this.snackbarService.showSuccessWithoutAction(
          'Connexion établie avec succès!'
        );
        this.authService.onLoginSuccessful(data);
      },
      error: (error: HttpErrorResponse) => {
        this.snackbarService.showErrorWithoutAction(
          `Erreur! Veuillez réessayer!`
        );
      },
    });
  }

  onSignUpClicked(event: MouseEvent): void {
    event.preventDefault();
    this.routerService.navigateToSignUp();
  }

  get user() {
    return this.loginForm.get('user');
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }
}
