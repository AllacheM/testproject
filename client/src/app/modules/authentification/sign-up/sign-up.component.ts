import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { IHttpResponse } from 'src/app/interfaces/http.interface';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MatDialogService } from 'src/app/services/mat-dialog/mat-dialog.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import { InformationDialogComponent } from 'src/app/shared-components/mat-dialogs/information-dialog/information-dialog.component';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {
  hidePwd = true;
  hideConfirmPwd = true;
  eyeHideIcon = faEyeSlash;
  eyeShowIcon = faEye;

  public signUpForm!: FormGroup;

  constructor(
    private dialogService: MatDialogService,
    private snackbarService: SnackbarService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.signUpForm = new FormGroup(
      {
        id: new FormControl('', [Validators.required]),
        email: new FormControl('', [Validators.required, Validators.email]),
        pwd: new FormControl('', [
          Validators.required,
          Validators.minLength(8),
        ]),
        confirmPwd: new FormControl('', [Validators.required]),
      },
      [CustomValidators.matchingPassword('pwd', 'confirmPwd')]
    );
  }

  openInfoDialog(): void {
    this.dialogService.open(InformationDialogComponent);
  }

  get id() {
    return this.signUpForm.get('id')!;
  }

  get email() {
    return this.signUpForm.get('email')!;
  }

  get pwd() {
    return this.signUpForm.get('pwd')!;
  }

  get confirmPwd() {
    return this.signUpForm.get('confirmPwd')!;
  }

  onHidePassword(event: MouseEvent, hide: boolean): boolean {
    event.preventDefault();
    return !hide;
  }

  createAccount(event: Event): void {
    event.preventDefault();

    if (!this.email?.value || !this.pwd?.value || !this.confirmPwd?.value) {
      this.snackbarService.showErrorWithoutAction(
        `Veuillez entrer un adresse courriel, un mot de passe et confirmez votre mot de passe!`
      );
      return;
    }

    this.authService
      .createUser(this.id?.value, this.email?.value, this.pwd?.value)
      .subscribe({
        next: (response: HttpResponse<IHttpResponse<any>>) => {
          const data = response.body;

          if (data?.code !== 1) {
            this.snackbarService.showErrorWithoutAction(data?.message!);
            return;
          }

          this.snackbarService.showSuccessWithoutAction(
            'Compte créé et connecté avec succès!'
          );

          console.log('AAAAAA');
          this.authService.onLoginSuccessful(data);
        },
        error: (error: HttpErrorResponse) => {
          this.snackbarService.showErrorWithoutAction(
            `Erreur! Veuillez réessayer!`
          );
        },
      });
  }
}

export class CustomValidators {
  static matchingPassword(newPass: string, confirmPass: string): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return control.get(newPass) &&
        control.get(confirmPass) &&
        control.get(newPass)?.value !== control.get(confirmPass)?.value
        ? { passwordNotMatch: true }
        : null;
    };
  }
}
