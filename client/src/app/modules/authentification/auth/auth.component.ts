import { Component, OnInit } from '@angular/core';
import { RouterService } from 'src/app/services/router/router.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  constructor(private routerServicer: RouterService) { 
    this.routerServicer.navigateToLogin();
  }

  ngOnInit(): void {
  }

}
