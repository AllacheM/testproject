import { Component, OnInit } from '@angular/core';
import { faHammer } from '@fortawesome/free-solid-svg-icons';
import { RouterService } from 'src/app/services/router/router.service';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ChangePasswordBSComponent } from 'src/app/shared-components/bottom-sheets/change-password-bs/change-password-bs.component';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-main-app',
  templateUrl: './main-app.component.html',
  styleUrls: ['./main-app.component.scss'],
})
export class MainAppComponent implements OnInit {
  faHammer = faHammer;

  goToAdminCb!: () => void;
  onLogOutCb!: () => void;
  goToProfileCb!: () => void;

  constructor(private routerService: RouterService,
              private passwordBottomSheet: MatBottomSheet,
              private userService: UserService) {}

  ngOnInit(): void {
    this.goToAdminCb = this.goToAdmin.bind(this);
    this.onLogOutCb = this.onLogOut.bind(this);
    this.goToProfileCb = this.goToProfile.bind(this);
  }

  isHome(): boolean {
    return this.routerService.isHome();
  }

  goToAdmin(): void {
    this.routerService.navigateToAdmin();
  }

  isAdmin(): boolean {
    return this.userService.isAdmin;
  }

  goToProfile(): void{}

  onLogOut(): void {}

  openPasswordBottomSheet(): void {
    this.passwordBottomSheet.open(ChangePasswordBSComponent);
  }
}
