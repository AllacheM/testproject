import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ITreeData } from 'src/app/interfaces/d3-tree.interface';
import { IHttpResponse } from 'src/app/interfaces/http.interface';
import { ProjectCreationService } from 'src/app/services/project-creation/project-creation.service';
import { ProjectTreeService } from 'src/app/services/project-tree/project-tree/project-tree.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import { NewPostitDialogComponent } from 'src/app/shared-components/mat-dialogs/new-postit-dialog/new-postit-dialog.component';

@Component({
  selector: 'app-project-creation',
  templateUrl: './project-creation.component.html',
  styleUrls: ['./project-creation.component.scss'],
})
export class ProjectCreationComponent implements OnInit {
  projectCreationForm: FormGroup;
  isMakingRequest = false;

  constructor(
    private projectCreationService: ProjectCreationService,
    private dialogRef: MatDialogRef<NewPostitDialogComponent>,
    private snackbarService: SnackbarService,
    private projectTreeService: ProjectTreeService
  ) {
    this.projectCreationForm = new FormGroup({
      fonction: new FormControl('', [Validators.required]),
      question: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {}

  close() {
    this.dialogRef.close();
  }

  createProjectClicked(event: MouseEvent) {
    event.preventDefault();

    this.isMakingRequest = true;
    this.projectCreationService
      .createRootProject(
        this.projectCreationForm.get('fonction')?.value,
        this.projectCreationForm.get('question')?.value
      )
      .subscribe({
        next: (response: HttpResponse<IHttpResponse<any>>) => {
          const data = response.body;

          if (data?.code !== 1) {
            this.snackbarService.showErrorWithoutAction(
              'Échec lors de la création du projet'
            );
            return;
          }

          const tree = data.data.tree as ITreeData;
          this.projectTreeService.addProjectTree(tree);
          this.close();
          this.isMakingRequest = false;
          this.snackbarService.showSuccessWithoutAction(`Nouveau projet créé`);
        },
        error: () => {
          this.isMakingRequest = false;
        },
      });
  }
}
