import { Component, OnInit } from '@angular/core';
import { faSmileWink } from '@fortawesome/free-regular-svg-icons';
import { ITreeData } from 'src/app/interfaces/d3-tree.interface';
import { POSTIT_CSS_CLASSES } from 'src/app/interfaces/matrix/project-matrix.interface';
import { MatDialogService } from 'src/app/services/mat-dialog/mat-dialog.service';
import { ProjectTreeService } from 'src/app/services/project-tree/project-tree/project-tree.service';
import { UserService } from 'src/app/services/user/user.service';
import { ProjectCreationComponent } from '../project-creation/project-creation.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  projectTrees: ITreeData[] = [];
  handsIcon = faSmileWink;
  isMakingRequest: boolean = true;

  private subscriptions: Subscription[] = [];

  constructor(
    private projectTreeService: ProjectTreeService,
    private userService: UserService,
    private dialogService: MatDialogService
  ) {}

  ngOnInit(): void {
    this.projectTreeService.fetchProjectTrees();
    this.handleSubscriptions();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
  getUsername(): string {
    return this.userService.username;
  }

  openCreateProjectDialog(): void {
    this.dialogService.open(ProjectCreationComponent);
  }

  getLegend() {
    return [
      {
        cssClass: POSTIT_CSS_CLASSES.reddish,
        title: 'Fonction racine',
      },
      {
        cssClass: POSTIT_CSS_CLASSES.orange,
        title: 'Cartographie',
      },
    ];
  }

  private handleSubscriptions(): void {
    this.subscriptions.push(
      this.projectTreeService.subjectProjectTreesObs.subscribe(
        (projectTrees: ITreeData[]) => {
          this.isMakingRequest = false;
          this.projectTrees = projectTrees;
          this.projectTreeService.drawTrees();
        }
      )
    );
  }
}
