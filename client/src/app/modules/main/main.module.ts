import { NgModule } from '@angular/core';
import { MainAuthGuard } from 'src/app/classes/auth-guards/main-auth.guard';
import { SharedModule } from '../shared-module';
import { MainAppComponent } from './main-app/main-app.component';
import { MainRoutingModule } from './main.routing.module';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { ProjectCreationComponent } from './project-creation/project-creation.component';
import { MatrixDrawingSocketService } from 'src/app/services/socket/matrix-drawing-socket/matrix-drawing-socket.service';

@NgModule({
  declarations: [
    MainAppComponent,
    HomeComponent,
    ProfileComponent,
    ProjectCreationComponent,
  ],
  imports: [SharedModule, MainRoutingModule],
  providers: [MainAuthGuard],
  bootstrap: [],
})
export class MainModule {
  constructor(private matrixDrawingSocketService: MatrixDrawingSocketService) {}
}
