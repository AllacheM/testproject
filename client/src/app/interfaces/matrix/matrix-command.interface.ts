export interface IMatrixCommand {
  execute(): void;
}
