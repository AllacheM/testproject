export interface IContextMenu<T> {
  title?: string;
  action?: (data: T, event: PointerEvent) => void;
  divider?: boolean;
}
