export interface ICartograpy {
  id: string;
  question: string;
  nbSeconds: number;
}

export interface IProjectMatrix {
  cartography: ICartograpy;
  root?: IPostit;
  postits: IPostit[];
}

export enum POSTIT_CSS_CLASSES {
  orange = 'orange-postit',
  reddish = 'redish-postit',
  green = 'green-postit',
}

export interface IPostit {
  id: number;
  zone: number;
  x: number; // relative x to the zone
  y: number; // relative y to the zone
  text: string;
  isRoot: boolean;
  cssClass: POSTIT_CSS_CLASSES;
}
