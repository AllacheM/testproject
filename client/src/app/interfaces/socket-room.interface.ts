export interface ISocketRoom {
  jwt: string;
  roomName: string;
}

export interface ISocketDataEmitter {
  jwt: string;
  data: any;
  roomName: string;
}

export interface ISocketDataListener<T> {
  code: number;
  data: T;
}
