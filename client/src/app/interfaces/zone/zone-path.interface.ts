export enum IZoneTextDirection {
  MIDDLE,
  TOP_LEFT,
  TOP_MIDDLE,
  TOP_RIGHT,
  // MIDDLE_LEFT,
  // MIDDLE_BOTTOM,
  // MIDDLE_RIGHT,
  // BOTTOM_LEFT,
  // BOTTOM_MIDDLE,
  // BOTTOM_RIGHT,
}

export interface IOrgan {
  index: number;
  zones: number[];
}

export interface IZone {
  index: number;
  text: IZoneText;
  color: string;
  path: IZoneCoords[][];
  isFunction?: boolean;
}

export interface IZoneText {
  name: string;
  direction: IZoneTextDirection;
}

export interface IZoneCoords {
  x0: number;
  x1: number;
  y0: number;
  y1: number;
}
