import { IOrgan, IZone, IZoneCoords } from './zone-path.interface';

export interface IZoneStrategy {
  getAreaData: () => IZone[];
  getLineData: () => IZoneCoords[];
  getZoneWidthSize: () => number;
  getZoneHeightSize: () => number;
  getOrgans: () => IOrgan[];
}

export interface IZonePositionCalculator {
  getColumn: (index: number) => number;
  getRow: (index: number) => number;
}
