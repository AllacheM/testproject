import { BaseType, HierarchyNode, HierarchyPointNode } from 'd3';
import { POSTIT_CSS_CLASSES } from './matrix/project-matrix.interface';

export interface ITreeData {
  id: number;
  text: string;
  cssClass: POSTIT_CSS_CLASSES;
  children?: ITreeData[];
}

export type ITreeRoot = HierarchyNode<ITreeData> & {
  x0: number;
  y0: number;
};

export type ITreeNode = HierarchyPointNode<ITreeData>;

export type ITreeGElement = d3.Selection<
  SVGGElement,
  HierarchyPointNode<ITreeData>,
  SVGGElement,
  unknown
>;

export type ITreeDiv = d3.Selection<
  BaseType,
  HierarchyPointNode<ITreeData>,
  SVGGElement,
  unknown
>;
