export interface IEnvironment {
  isProduction: boolean;
  serverURL: string;
  serverBaseURL: string;
}
