export interface ICompany {
    id: string;
    securityCode: string;
    name: string;
    info: string;
  }