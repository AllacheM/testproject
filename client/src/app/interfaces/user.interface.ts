export type IUserRole = 'admin' | 'participant' | 'animator';

export interface IUser {
  id: string;
  email: string;
  name: string;
  role: IUserRole;
}
