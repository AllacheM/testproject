import {
  IZone,
  IZoneCoords,
  IZoneTextDirection,
} from 'src/app/interfaces/zone/zone-path.interface';
import { AbsZone } from './abs-zone';

export abstract class AbsAreaCalculator extends AbsZone {
  constructor(zoneWidthSize: number, zoneHeightSize: number) {
    super(zoneWidthSize, zoneHeightSize);
  }

  abstract get zone1(): IZoneCoords[][];
  abstract get zone2(): IZoneCoords[][];
  abstract get zone3(): IZoneCoords[][];
  abstract get zone4(): IZoneCoords[][];
  abstract get zone5(): IZoneCoords[][];
  abstract get zone6(): IZoneCoords[][];
  abstract get zone7(): IZoneCoords[][];
  abstract get zone8(): IZoneCoords[][];
  abstract get zone9(): IZoneCoords[][];
  abstract get zone10(): IZoneCoords[][];
  abstract get zone11(): IZoneCoords[][];
  abstract get zone12(): IZoneCoords[][];
  abstract get zone13(): IZoneCoords[][];
  abstract get zone14(): IZoneCoords[][];
  abstract get zone15(): IZoneCoords[][];
  abstract get zone16(): IZoneCoords[][];
  abstract get zone17(): IZoneCoords[][];
  abstract get zone18(): IZoneCoords[][];
  abstract get zone19(): IZoneCoords[][];
  abstract get zone20(): IZoneCoords[][];
  abstract get zone21(): IZoneCoords[][];
  abstract get zone22(): IZoneCoords[][];
  abstract get zone23(): IZoneCoords[][];
  abstract get zone24(): IZoneCoords[][];

  get areas(): IZone[] {
    return [
      {
        index: 1,
        text: {
          name: 'Croyances Sociales',
          direction: IZoneTextDirection.TOP_LEFT,
        },
        color: '#e7eae1',
        path: this.zone1,
      },
      {
        index: 2,
        text: {
          name: 'Enjeux Sociaux',
          direction: IZoneTextDirection.TOP_MIDDLE,
        },
        color: '#e9eddc',
        path: this.zone2,
      },
      {
        index: 3,
        text: {
          name: 'Aspirations Sociales',
          direction: IZoneTextDirection.TOP_RIGHT,
        },
        color: '#e7eae1',
        path: this.zone3,
      },
      {
        index: 4,
        text: {
          name: 'Utilités Sociales',
          direction: IZoneTextDirection.TOP_RIGHT,
        },
        color: '#e7e1ed',
        path: this.zone4,
      },
      {
        index: 5,
        text: {
          name: 'Initiatives Sociales',
          direction: IZoneTextDirection.TOP_RIGHT,
        },
        color: '#f6e5de',
        path: this.zone5,
      },
      {
        index: 6,
        text: {
          name: 'Structures Sociales',
          direction: IZoneTextDirection.TOP_MIDDLE,
        },
        color: '#fdeadc',
        path: this.zone6,
      },
      {
        index: 7,
        text: {
          name: 'Savoirs Humains',
          direction: IZoneTextDirection.TOP_LEFT,
        },
        color: '#f6e5de',
        path: this.zone7,
      },
      {
        index: 8,
        text: {
          name: 'Ressources Existantes',
          direction: IZoneTextDirection.TOP_LEFT,
        },
        color: '#e7e1ed',
        path: this.zone8,
      },
      {
        index: 9,
        text: {
          name: 'Croyances Sectorielles',
          direction: IZoneTextDirection.TOP_LEFT,
        },
        color: '#eaede6',
        path: this.zone9,
      },
      {
        index: 10,
        text: {
          name: 'Enjeux Stratégiques',
          direction: IZoneTextDirection.TOP_MIDDLE,
        },
        color: '#eff1e4',
        path: this.zone10,
      },
      {
        index: 11,
        text: {
          name: 'Aspiration Sectorielle',
          direction: IZoneTextDirection.TOP_RIGHT,
        },
        color: '#eaede4',
        path: this.zone11,
      },
      {
        index: 12,
        text: {
          name: 'Besoins Perçus',
          direction: IZoneTextDirection.TOP_RIGHT,
        },
        color: '#ece6f2',
        path: this.zone12,
      },
      {
        index: 13,
        text: {
          name: 'Initiatives Sectorielles',
          direction: IZoneTextDirection.TOP_RIGHT,
        },
        color: '#f7e8e5',
        path: this.zone13,
      },
      {
        index: 14,
        text: {
          name: 'Structures Sectorielle',
          direction: IZoneTextDirection.TOP_MIDDLE,
        },
        color: '#fdeedf',
        path: this.zone14,
      },
      {
        index: 15,
        text: {
          name: 'Pratiques Sectorielles',
          direction: IZoneTextDirection.TOP_LEFT,
        },
        color: '#f7e8e5',
        path: this.zone15,
      },
      {
        index: 16,
        text: {
          name: 'Ressources Accessibles',
          direction: IZoneTextDirection.TOP_LEFT,
        },
        color: '#ece6f2',
        path: this.zone16,
      },
      {
        index: 17,
        text: {
          name: 'Croyances Partagées',
          direction: IZoneTextDirection.TOP_LEFT,
        },
        color: '#f0f3ec',
        path: this.zone17,
      },
      {
        index: 18,
        text: {
          name: 'Stratégies',
          direction: IZoneTextDirection.TOP_MIDDLE,
        },
        color: '#f5f8ef',
        path: this.zone18,
      },
      {
        index: 19,
        text: {
          name: 'Aspiration Partagées',
          direction: IZoneTextDirection.TOP_RIGHT,
        },
        color: '#f0f3ec',
        path: this.zone19,
      },
      {
        index: 20,
        text: {
          name: 'Demandes Exprimées',
          direction: IZoneTextDirection.TOP_RIGHT,
        },
        color: '#f5f0f6',
        path: this.zone20,
      },
      {
        index: 21,
        text: {
          name: 'Initiatives Organisationnelles',
          direction: IZoneTextDirection.TOP_RIGHT,
        },
        color: '#f9efed',
        path: this.zone21,
      },
      {
        index: 22,
        text: {
          name: 'Structures Techniques',
          direction: IZoneTextDirection.TOP_MIDDLE,
        },
        color: '#fdf4ef',
        path: this.zone22,
      },
      {
        index: 23,
        text: {
          name: 'Savoir-faire Organisationnelles',
          direction: IZoneTextDirection.TOP_LEFT,
        },
        color: '#f9efed',
        path: this.zone23,
      },
      {
        index: 24,
        text: {
          name: 'Ressources Assurées',
          direction: IZoneTextDirection.TOP_LEFT,
        },
        color: '#f5f0f6',
        path: this.zone24,
      },
      {
        index: 25,
        text: {
          name: 'Fonction',
          direction: IZoneTextDirection.TOP_MIDDLE,
        },
        color: '#fff',
        isFunction: true,
        path: [
          [
            {
              x0: this.getColumn(3),
              x1: this.getColumn(4),
              y0: this.getRow(3),
              y1: this.getRow(3),
            },
            {
              x0: this.getColumn(3),
              x1: this.getColumn(4),
              y0: this.getRow(4),
              y1: this.getRow(4),
            },
          ],
        ],
      },
    ];
  }
}
