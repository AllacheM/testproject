import { AbsAreaCalculator } from '../../abs-area-calculator';

export class ThreeZoneAreaCalculator extends AbsAreaCalculator {
  // 3 * 3
  constructor(zoneWidthSize: number, zoneHeightSize: number) {
    super(zoneWidthSize, zoneHeightSize);
  }

  get zone1() {
    return [
      [
        { x0: 0, x1: this.getColumn(2), y0: 0, y1: 0 },
        {
          x0: 0,
          x1: this.getColumn(2),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
      ],
      [
        { x0: 0, x1: this.getColumn(1), y0: 0, y1: 0 },
        {
          x0: 0,
          x1: this.getColumn(1),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
      ],
    ];
  }

  get zone2() {
    return [
      [
        { x0: this.getColumn(0), x1: this.getColumn(3), y0: 0, y1: 0 },
        {
          x0: this.getColumn(0),
          x1: this.getColumn(3),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
      ],
    ];
  }

  get zone3() {
    return [
      [
        { x0: this.getColumn(1), x1: this.getColumn(3), y0: 0, y1: 0 },
        {
          x0: this.getColumn(1),
          x1: this.getColumn(3),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
      ],
      [
        { x0: this.getColumn(2), x1: this.getColumn(3), y0: 0, y1: 0 },
        {
          x0: this.getColumn(2),
          x1: this.getColumn(3),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
      ],
    ];
  }

  get zone4() {
    return [
      [
        {
          x0: this.getColumn(2),
          x1: this.getColumn(3),
          y0: this.getRow(0),
          y1: this.getRow(0),
        },
        {
          x0: this.getColumn(2),
          x1: this.getColumn(3),
          y0: this.getRow(3),
          y1: this.getRow(3),
        },
      ],
    ];
  }

  get zone5() {
    return [
      [
        {
          x0: this.getColumn(2),
          x1: this.getColumn(3),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
        {
          x0: this.getColumn(2),
          x1: this.getColumn(3),
          y0: this.getRow(3),
          y1: this.getRow(3),
        },
      ],
      [
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(3),
          y1: this.getRow(3),
        },
      ],
    ];
  }

  get zone6() {
    return [
      [
        {
          x0: this.getColumn(0),
          x1: this.getColumn(3),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
        {
          x0: this.getColumn(0),
          x1: this.getColumn(3),
          y0: this.getRow(3),
          y1: this.getRow(3),
        },
      ],
    ];
  }

  get zone7() {
    return [
      [
        {
          x0: this.getColumn(0),
          x1: this.getColumn(1),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
        {
          x0: this.getColumn(0),
          x1: this.getColumn(1),
          y0: this.getRow(3),
          y1: this.getRow(3),
        },
      ],
      [
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(3),
          y1: this.getRow(3),
        },
      ],
    ];
  }

  get zone8() {
    return [
      [
        {
          x0: 0,
          x1: this.getColumn(1),
          y0: this.getRow(0),
          y1: this.getRow(0),
        },
        {
          x0: 0,
          x1: this.getColumn(1),
          y0: this.getRow(3),
          y1: this.getRow(3),
        },
      ],
    ];
  }

  get zone9() {
    return [
      [
        {
          x0: this.getColumn(1),
          x1: this.getColumn(3),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
        {
          x0: this.getColumn(1),
          x1: this.getColumn(3),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
      ],
      [
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(3),
          y1: this.getRow(3),
        },
      ],
    ];
  }

  get zone10() {
    return [
      [
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
      ],
    ];
  }

  get zone11() {
    return [
      [
        {
          x0: this.getColumn(0),
          x1: this.getColumn(2),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
        {
          x0: this.getColumn(0),
          x1: this.getColumn(2),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
      ],
      [
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(3),
          y1: this.getRow(3),
        },
      ],
    ];
  }

  get zone12() {
    return [
      [
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
      ],
    ];
  }

  get zone13() {
    return [
      [
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(0),
          y1: this.getRow(0),
        },
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
      ],
      [
        {
          x0: this.getColumn(0),
          x1: this.getColumn(1),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
        {
          x0: this.getColumn(0),
          x1: this.getColumn(1),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
      ],
    ];
  }

  get zone14() {
    return [
      [
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
      ],
    ];
  }

  get zone15() {
    return [
      [
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(0),
          y1: this.getRow(0),
        },
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
      ],
      [
        {
          x0: this.getColumn(2),
          x1: this.getColumn(3),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
        {
          x0: this.getColumn(2),
          x1: this.getColumn(3),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
      ],
    ];
  }

  get zone16() {
    return [
      [
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
      ],
    ];
  }

  get zone17() {
    return [
      [
        {
          x0: this.getColumn(2),
          x1: this.getColumn(3),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
        {
          x0: this.getColumn(2),
          x1: this.getColumn(3),
          y0: this.getRow(3),
          y1: this.getRow(3),
        },
      ],
    ];
  }

  get zone18() {
    return [
      [
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(3),
          y1: this.getRow(3),
        },
      ],
    ];
  }

  get zone19() {
    return [
      [
        {
          x0: this.getColumn(0),
          x1: this.getColumn(1),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
        {
          x0: this.getColumn(0),
          x1: this.getColumn(1),
          y0: this.getRow(3),
          y1: this.getRow(3),
        },
      ],
    ];
  }

  get zone20() {
    return [
      [
        {
          x0: this.getColumn(0),
          x1: this.getColumn(1),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
        {
          x0: this.getColumn(0),
          x1: this.getColumn(1),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
      ],
    ];
  }

  get zone21() {
    return [
      [
        {
          x0: this.getColumn(0),
          x1: this.getColumn(1),
          y0: this.getRow(0),
          y1: this.getRow(0),
        },
        {
          x0: this.getColumn(0),
          x1: this.getColumn(1),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
      ],
    ];
  }

  get zone22() {
    return [
      [
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(0),
          y1: this.getRow(0),
        },
        {
          x0: this.getColumn(1),
          x1: this.getColumn(2),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
      ],
    ];
  }

  get zone23() {
    return [
      [
        {
          x0: this.getColumn(2),
          x1: this.getColumn(3),
          y0: this.getRow(0),
          y1: this.getRow(0),
        },
        {
          x0: this.getColumn(2),
          x1: this.getColumn(3),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
      ],
    ];
  }

  get zone24() {
    return [
      [
        {
          x0: this.getColumn(2),
          x1: this.getColumn(3),
          y0: this.getRow(1),
          y1: this.getRow(1),
        },
        {
          x0: this.getColumn(2),
          x1: this.getColumn(3),
          y0: this.getRow(2),
          y1: this.getRow(2),
        },
      ],
    ];
  }

  get zone25Function() {
    return [
      [
        {
          x0: this.getColumn(3),
          x1: this.getColumn(4),
          y0: this.getRow(3),
          y1: this.getRow(3),
        },
        {
          x0: this.getColumn(3),
          x1: this.getColumn(4),
          y0: this.getRow(4),
          y1: this.getRow(4),
        },
      ],
    ];
  }
}
