import { ORGANS } from 'src/app/declaration/matrix/organs';
import {
  IOrgan,
  IZone,
  IZoneCoords,
} from 'src/app/interfaces/zone/zone-path.interface';
import { IZoneStrategy } from 'src/app/interfaces/zone/zone-strategy.interface';
import { ThreeZoneAreaCalculator } from './3-zone-area-calculator';
import { ThreeZoneLineCalculator } from './3-zone-line-calculator';

export class ThreeZones implements IZoneStrategy {
  private readonly nbSquares = 3;
  private zoneWidthSize!: number;
  private zoneHeightSize!: number;
  private organ!: IOrgan;

  constructor(organIndex: number, width: number, height: number) {
    this.zoneWidthSize = width / this.nbSquares;
    this.zoneHeightSize = height / this.nbSquares;
    this.setOrgan(organIndex);
  }

  setOrgan(index: number): void {
    this.organ = ORGANS.filter((organ) => organ.index === index)[0];
  }

  getOrgans(): IOrgan[] {
    return [this.organ];
  }

  getZoneWidthSize(): number {
    return this.zoneWidthSize;
  }

  getZoneHeightSize(): number {
    return this.zoneHeightSize;
  }

  getLineData(): IZoneCoords[] {
    const zoneLineCalculator = new ThreeZoneLineCalculator(
      this.zoneWidthSize,
      this.zoneHeightSize
    );

    return zoneLineCalculator.getLines(this.organ.index);
  }

  getAreaData(): IZone[] {
    const zoneAreaCalculator = new ThreeZoneAreaCalculator(
      this.zoneWidthSize,
      this.zoneHeightSize
    );

    const areas = zoneAreaCalculator.areas.filter((zone) =>
      this.organ.zones.includes(zone.index)
    );

    return areas;
  }
}
