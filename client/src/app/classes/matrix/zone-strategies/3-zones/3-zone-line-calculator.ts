import { IZoneCoords } from 'src/app/interfaces/zone/zone-path.interface';
import { AbsZone } from '../../abs-zone';

export class ThreeZoneLineCalculator extends AbsZone {
  constructor(zoneWidthSize: number, zoneHeightSize: number) {
    super(zoneWidthSize, zoneHeightSize);
  }

  getLines(organIndex: number): IZoneCoords[] {
    switch (organIndex) {
      case 1:
        return this.line1;
      default:
        return [];
    }
  }

  private get line1() {
    return [
      {
        x0: this.getColumn(0),
        x1: this.getColumn(2),
        y0: this.getRow(0),
        y1: this.getRow(0),
      },
      {
        x0: this.getColumn(0),
        x1: this.getColumn(0),
        y0: this.getRow(0),
        y1: this.getRow(2),
      },
      {
        x0: this.getColumn(1),
        x1: this.getColumn(3),
        y0: this.getRow(1),
        y1: this.getRow(1),
      },
      {
        x0: this.getColumn(0),
        x1: this.getColumn(1),
        y0: this.getRow(2),
        y1: this.getRow(2),
      },
      {
        x0: this.getColumn(1),
        x1: this.getColumn(1),
        y0: this.getRow(1),
        y1: this.getRow(3),
      },
      {
        x0: this.getColumn(3),
        x1: this.getColumn(3),
        y0: this.getRow(1),
        y1: this.getRow(3),
      },
      {
        x0: this.getColumn(1),
        x1: this.getColumn(3),
        y0: this.getRow(3),
        y1: this.getRow(3),
      },
      {
        x0: this.getColumn(2),
        x1: this.getColumn(3),
        y0: this.getRow(2),
        y1: this.getRow(2),
      },
      {
        x0: this.getColumn(2),
        x1: this.getColumn(2),
        y0: this.getRow(2),
        y1: this.getRow(3),
      },
      {
        x0: this.getColumn(2),
        x1: this.getColumn(2),
        y0: this.getRow(0),
        y1: this.getRow(1),
      },
    ];
  }
}
