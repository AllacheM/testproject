import { ORGANS } from 'src/app/declaration/matrix/organs';
import {
  IOrgan,
  IZone,
  IZoneCoords,
} from 'src/app/interfaces/zone/zone-path.interface';
import { IZoneStrategy } from 'src/app/interfaces/zone/zone-strategy.interface';
import { TwentySevenZoneAreaCalculator } from './27-zone-area-calculator';
import { TwentySevenZoneLineCalculator } from './27-zone-line-calculator';

export class TwentySevenZones implements IZoneStrategy {
  private readonly nbSquares = 7;
  private zoneWidthSize!: number;
  private zoneHeightSize!: number;

  constructor(width: number, height: number) {
    this.zoneWidthSize = width / this.nbSquares;
    this.zoneHeightSize = height / this.nbSquares;
  }

  getZoneWidthSize(): number {
    return this.zoneWidthSize;
  }

  getZoneHeightSize(): number {
    return this.zoneHeightSize;
  }

  getLineData(): IZoneCoords[] {
    const zoneLineCalculator = new TwentySevenZoneLineCalculator(
      this.zoneWidthSize,
      this.zoneHeightSize
    );

    return zoneLineCalculator.lines;
  }

  getAreaData(): IZone[] {
    const zoneAreaCalculator = new TwentySevenZoneAreaCalculator(
      this.zoneWidthSize,
      this.zoneHeightSize
    );

    return zoneAreaCalculator.areas;
  }

  getOrgans(): IOrgan[] {
    return ORGANS;
  }
}
