import { IZoneCoords } from 'src/app/interfaces/zone/zone-path.interface';
import { AbsZone } from '../../abs-zone';

export class TwentySevenZoneLineCalculator extends AbsZone {
  constructor(zoneWidthSize: number, zoneHeightSize: number) {
    super(zoneWidthSize, zoneHeightSize);
  }

  get lines(): IZoneCoords[] {
    return [
      // 4 sides
      { x0: 0, x1: this.getColumn(7), y0: 0, y1: 0 },
      { x0: 0, x1: 0, y0: 0, y1: this.getRow(7) },
      {
        x0: this.getColumn(7),
        x1: this.getColumn(7),
        y0: 0,
        y1: this.getRow(7),
      },
      { x0: 0, x1: this.getColumn(7), y0: this.getRow(7), y1: this.getRow(7) },

      // inner zones
      {
        x0: this.getColumn(2),
        x1: this.getColumn(2),
        y0: this.getRow(0),
        y1: this.getRow(1),
      },
      {
        x0: this.getColumn(5),
        x1: this.getColumn(5),
        y0: this.getRow(0),
        y1: this.getRow(1),
      },
      {
        x0: this.getColumn(2),
        x1: this.getColumn(2),
        y0: this.getRow(6),
        y1: this.getRow(7),
      },
      {
        x0: this.getColumn(5),
        x1: this.getColumn(5),
        y0: this.getRow(6),
        y1: this.getRow(7),
      },
      {
        x0: this.getColumn(1),
        x1: this.getColumn(1),
        y0: this.getRow(1),
        y1: this.getRow(6),
      },
      {
        x0: this.getColumn(6),
        x1: this.getColumn(6),
        y0: this.getRow(1),
        y1: this.getRow(6),
      },
      {
        x0: this.getColumn(1),
        x1: this.getColumn(6),
        y0: this.getRow(1),
        y1: this.getRow(1),
      },
      {
        x0: this.getColumn(1),
        x1: this.getColumn(6),
        y0: this.getRow(6),
        y1: this.getRow(6),
      },
      {
        x0: this.getColumn(0),
        x1: this.getColumn(1),
        y0: this.getRow(2),
        y1: this.getRow(2),
      },
      {
        x0: this.getColumn(6),
        x1: this.getColumn(7),
        y0: this.getRow(2),
        y1: this.getRow(2),
      },
      {
        x0: this.getColumn(0),
        x1: this.getColumn(1),
        y0: this.getRow(5),
        y1: this.getRow(5),
      },
      {
        x0: this.getColumn(6),
        x1: this.getColumn(7),
        y0: this.getRow(5),
        y1: this.getRow(5),
      },
      {
        x0: this.getColumn(3),
        x1: this.getColumn(3),
        y0: this.getRow(1),
        y1: this.getRow(6),
      },
      {
        x0: this.getColumn(4),
        x1: this.getColumn(4),
        y0: this.getRow(1),
        y1: this.getRow(6),
      },
      {
        x0: this.getColumn(2),
        x1: this.getColumn(2),
        y0: this.getRow(2),
        y1: this.getRow(5),
      },
      {
        x0: this.getColumn(5),
        x1: this.getColumn(5),
        y0: this.getRow(2),
        y1: this.getRow(5),
      },
      {
        x0: this.getColumn(2),
        x1: this.getColumn(5),
        y0: this.getRow(2),
        y1: this.getRow(2),
      },
      {
        x0: this.getColumn(2),
        x1: this.getColumn(5),
        y0: this.getRow(5),
        y1: this.getRow(5),
      },
      {
        x0: this.getColumn(1),
        x1: this.getColumn(6),
        y0: this.getRow(3),
        y1: this.getRow(3),
      },
      {
        x0: this.getColumn(1),
        x1: this.getColumn(6),
        y0: this.getRow(4),
        y1: this.getRow(4),
      },
    ];
  }
}
