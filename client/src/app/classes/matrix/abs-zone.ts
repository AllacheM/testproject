import { IZonePositionCalculator } from 'src/app/interfaces/zone/zone-strategy.interface';

export abstract class AbsZone implements IZonePositionCalculator {
  private zoneWidthSize!: number;
  private zoneHeightSize!: number;

  constructor(zoneWidthSize: number, zoneHeightSize: number) {
    this.zoneWidthSize = zoneWidthSize;
    this.zoneHeightSize = zoneHeightSize;
  }

  getColumn(index: number) {
    return this.zoneWidthSize * index;
  }

  getRow(index: number) {
    return this.zoneHeightSize * index;
  }
}
