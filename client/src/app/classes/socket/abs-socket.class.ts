import { io, Socket } from 'socket.io-client';
import { ISocketRoom } from 'src/app/interfaces/socket-room.interface';
import { environment } from 'src/environments/environment';
import {
  MATRIX_ROOM_EVENT_NAME,
  MATRIX_LEAVE_ROOM_EVENT_NAME,
} from '../../declaration/socket/socket-room.constants';

export abstract class AbsSocketService {
  readonly SERVER_URL: string = environment.serverURL;
  protected namespaceSocket!: Socket<any>;

  protected init() {
    this.namespaceSocket = io(this.SERVER_URL);
    this.disconnect();
    this.setSocketListens();
  }

  protected abstract setSocketListens(): void;

  joinRoom(room: ISocketRoom) {
    this.emit(MATRIX_ROOM_EVENT_NAME, room);
  }

  leaveRoom(room: ISocketRoom): void {
    this.emit(MATRIX_LEAVE_ROOM_EVENT_NAME, room);
  }

  connect(): void {
    this.namespaceSocket.connect();
  }

  disconnect(): void {
    this.namespaceSocket.disconnect();
  }

  protected emit(event: string, data: any): void {
    this.namespaceSocket.emit(event, data);
  }

  protected emitWithCallback(
    event: string,
    data: any,
    callback: (response: any) => any
  ): void {
    this.namespaceSocket.emit(event, data, callback);
  }
}
