export abstract class AbsLocalStorage {
  private key: string;

  constructor(key: string) {
    this.key = key;
  }

  get(): string | null {
    return localStorage.getItem(this.key);
  }

  set(value: string): void {
    localStorage.setItem(this.key, value);
  }

  remove(): void {
    localStorage.removeItem(this.key);
  }
}
