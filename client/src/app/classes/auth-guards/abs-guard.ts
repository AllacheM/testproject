import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { RouterService } from 'src/app/services/router/router.service';

@Injectable({
  providedIn: 'root',
})
export abstract class AbsAuthGuard implements CanActivate {
  constructor(
    protected routerService: RouterService,
    protected authService: AuthService
  ) {}

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!(await this.executeActivation())) {
      this.navigationCallback();
      return false;
    }

    return true;
  }

  protected abstract executeActivation(): Promise<boolean>;
  protected abstract navigationCallback(): void;
}
