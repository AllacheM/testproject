import { Injectable } from '@angular/core';
import { AbsAuthGuard } from './abs-guard';

@Injectable({
  providedIn: 'root',
})
export class AuthentificationGuard extends AbsAuthGuard {
  protected navigationCallback(): void {
    this.routerService.navigateToHome();
  }

  protected async executeActivation(): Promise<boolean> {
    return !(await this.authService.isAuthenticated());
  }
}
