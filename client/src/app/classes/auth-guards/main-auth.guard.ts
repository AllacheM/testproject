import { Injectable } from '@angular/core';
import { AbsAuthGuard } from './abs-guard';

@Injectable({
  providedIn: 'root',
})
export class MainAuthGuard extends AbsAuthGuard {
  protected navigationCallback(): void {
    this.routerService.navigateToLogin();
  }

  // return true if user is logged in and user role !== admin
  protected async executeActivation(): Promise<boolean> {
    return await this.authService.isAuthenticated();
  }
}
