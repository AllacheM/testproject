import { HierarchyPointNode, TreeLayout } from 'd3';
import * as d3 from 'd3';
import { d3SVG } from 'src/app/declaration/types/d3.types';
import {
  ITreeData,
  ITreeDiv,
  ITreeGElement,
  ITreeNode,
  ITreeRoot,
} from 'src/app/interfaces/d3-tree.interface';
import { POSTIT_CSS_CLASSES } from 'src/app/interfaces/matrix/project-matrix.interface';

export class D3Tree {
  private readonly rectHeight = 80;
  private readonly rectWidth = 120;
  private readonly linkHeight = 120;

  private width: number;
  private height: number;
  private data: ITreeData;
  private root!: ITreeRoot;
  private svg!: d3SVG;
  private onNodeClickedCb: (e: PointerEvent, d: ITreeNode) => void;
  private treeMap!: TreeLayout<ITreeData>;

  constructor(
    data: ITreeData,
    svg: d3SVG,
    width: number,
    height: number,
    onNodeClickedCb: (e: PointerEvent, d: ITreeNode) => void
  ) {
    this.data = data;
    this.width = width;
    this.height = height;
    this.svg = svg;
    this.onNodeClickedCb = onNodeClickedCb;

    this.initTree();
  }

  private initTree(): void {
    this.treeMap = d3
      .tree<ITreeData>()
      .separation((a, b) => 3)
      .size([this.height, this.width]);

    this.root = d3.hierarchy(this.data, function (d: ITreeData) {
      return d.children;
    }) as ITreeRoot;

    this.drawTree();
  }

  private drawTree(): void {
    const treeData = this.treeMap(this.root);
    const nodes = treeData.descendants();
    const links = treeData.descendants().slice(1);

    // adjust node positions
    nodes.forEach((d: ITreeNode) => {
      // d.x *= 2;
      d.y = d.depth * this.linkHeight;
    });

    const node = this.drawNode(nodes);
    const div = this.drawDiv(node);
    this.drawText(div);
    this.drawLinks(links);
    this.translateElements();
  }

  private isNodeRoot(d: ITreeNode): boolean {
    return d.depth === 0;
  }

  onNodeClicked(e: PointerEvent, node: ITreeNode): void {
    if (this.isNodeRoot(node)) {
      return;
    }

    this.onNodeClickedCb(e, node);
  }

  private drawNode(nodes: ITreeNode[]): ITreeGElement {
    return this.svg
      .selectAll('g.node')
      .data(nodes)
      .enter()
      .append('g')
      .attr('class', 'node')
      .attr('transform', (d: ITreeNode) => `translate(${d.x}, ${d.y})`)
      .on('click', (e, d) => this.onNodeClicked(e, d));
  }

  private drawDiv(node: ITreeGElement): ITreeDiv {
    return node
      .append('foreignObject')
      .attr('id', (d) => `fo-(${d.id})`)
      .attr('width', this.rectWidth)
      .attr('height', this.rectHeight)
      .attr('x', 0)
      .attr('y', (this.rectHeight / 2) * -1)
      .append('xhtml:div')
      .attr('width', this.rectWidth)
      .attr('height', this.rectHeight)
      .attr(
        'class',
        (d) =>
          `node-div ${
            this.isNodeRoot(d)
              ? d.data.cssClass
              : `${POSTIT_CSS_CLASSES.orange} node-div-clickable`
          }`
      );
  }

  private drawText(div: ITreeDiv): void {
    div.append('small').text((d: ITreeNode) => d.data.text);
  }

  private drawLinks(links: HierarchyPointNode<ITreeData>[]): void {
    this.svg
      .selectAll('path.link')
      .data(links)
      .enter()
      .insert('path', 'g')
      .attr('class', 'link')
      .attr('d', (d: ITreeNode) => {
        const child = d;
        const parent = d.parent!;

        return `M ${child.x + this.rectWidth / 2} ${child.y}
        C ${(child.x + parent.x) / 2 + this.rectWidth / 2} ${child.y},
          ${(child.x + parent.x) / 2 + this.rectWidth / 2} ${parent.y},
          ${parent.x + this.rectWidth / 2} ${parent.y}`;
      });
  }

  private translateElements(): void {
    this.svg.selectAll('foreignObject, path').each((_, d: any, n: any) => {
      d3.select(n[d]).attr(
        'transform',
        `translate(${0}, ${this.height / 2 - this.rectHeight})`
      );
    });
  }
}
