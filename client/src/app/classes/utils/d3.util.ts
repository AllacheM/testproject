import * as d3 from 'd3';
import { d3G } from 'src/app/declaration/types/d3.types';

export class D3Utils {
  // https://bl.ocks.org/mbostock/7555321
  // break a long text over multiple lines from a fixed width
  static wrapText(text: d3G, width: number) {
    text.each(function () {
      let text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line: string[] = [];

      const textBbox = (text.node() as SVGTextElement).getBBox();

      let tspan = text
        .text(null)
        .append('tspan')
        .attr('x', text.attr('x'))
        .attr('y', textBbox.y + textBbox.height) as any;

      while ((word = words.pop())) {
        line.push(word);
        tspan.text(line.join(' '));
        if (tspan.node().getComputedTextLength() > width) {
          line.pop();
          tspan.text(line.join(' '));
          line = [word];
          tspan = text
            .append('tspan')
            .attr('x', text.attr('x'))
            .attr('dy', parseFloat(text.attr('dy')) + 'em')
            .text(word);
        }
      }
    });
  }

  static getPostit(id: number): string {
    return `postit-${id}`;
  }

  static getPostitForeignObject(id: number): string {
    return `postit-foreign-obj-${id}`;
  }
}
