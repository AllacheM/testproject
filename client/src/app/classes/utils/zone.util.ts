import { IZoneTextDirection } from 'src/app/interfaces/zone/zone-path.interface';

export class ZoneUtils {
  // calculate the position of the text in a zone
  static getTextPosition(
    domRect: DOMRect,
    direction: IZoneTextDirection
  ): [number, number, string] {
    const offsetX = 10;
    const offsetY = 15;

    const anchorStart = 'start';
    const anchorMiddle = 'middle';
    const anchorEnd = 'end';

    switch (direction) {
      case IZoneTextDirection.TOP_LEFT:
        return [domRect.x + offsetX, domRect.y + offsetY, anchorStart];
      case IZoneTextDirection.TOP_MIDDLE:
        return [
          domRect.x + domRect.width / 2,
          domRect.y + offsetY,
          anchorMiddle,
        ];
      case IZoneTextDirection.TOP_RIGHT:
        return [
          domRect.x + domRect.width - offsetX,
          domRect.y + offsetY,
          anchorEnd,
        ];
      case IZoneTextDirection.MIDDLE:
        return [
          domRect.x + domRect.width / 2,
          domRect.y + domRect.height / 2,
          anchorMiddle,
        ];
    }
  }
}

export const CANVAS_CLASSES = {
  mainSvg: 'main-svg-canvas',
};

export const ZONE_CLASSES = {
  areaZone: 'area-zone',
  areaZoneId: 'area-zone-id',
  areaZonePath: 'area-zone-path',
  areaZoneName: 'area-zone-name',
};

export const POSTITS_CLASSES = {
  postitGContainer: 'postit-g-container',
  postitMainContainer: 'postit-main-container',
  postitHolder: 'postit-holder',
  postitText: 'postit-text',
  postitZoneNb: 'postit-zone-nb',
};

export const POSTITS_SIZE = {
  maxWidth: 150,
  maxHeight: 300,
};
