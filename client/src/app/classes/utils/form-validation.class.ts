import { AbstractControl } from '@angular/forms';

export class FormValidation {
  static validateEmail = (
    control: AbstractControl
  ): { [key: string]: boolean } | null => {
    if (
      !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        control.value
      )
    ) {
      return { emailError: true };
    }
    return null;
  };
}
