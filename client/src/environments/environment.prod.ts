import { IEnvironment } from 'src/app/interfaces/environment.interface';

export const environment: IEnvironment = {
  isProduction: true,
  serverBaseURL: '',
  serverURL: 'x/api',
};
