# PolyExpert Client

## Development server

Run `npm start` for a hot reload dev server. Chrome automatically navigates to `http://localhost:4200/`.

## Code scaffolding

Run `ng generate component component-name --module=MODULE_PATH` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.
